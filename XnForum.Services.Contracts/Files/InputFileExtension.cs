﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Contracts.Files
{
    public static class InputFileExtension
    {
        public static void SaveAs(this IInputFile file, string filename)
        {
            using (var fileStream = File.Create(filename))
            {
                file.InputStream.Seek(0, SeekOrigin.Begin);
                file.InputStream.CopyTo(fileStream);
            }
        }
    }
}
