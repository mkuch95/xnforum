﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.DTO.Message;

namespace XnForum.Services.Contracts.Interfaces
{
    public interface IMessageService
    {
        TalksListDto GetHeaderMessages(int userId, int page = 1);
        MessageTalkDto GetTalk(int userId, string userLogin, int page = 1);
        void AddMessage(MessageCreateDto message);
        void DeleteMessage(int userId, int messageId);//softdelete
    }
}
