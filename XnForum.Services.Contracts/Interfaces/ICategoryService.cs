﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.DTO;

namespace XnForum.Services.Contracts.Interfaces
{
    public interface ICategoryService
    {
        IList<CategoryDto> GetVisibleCategories();
        IList<CategoryDto> GetAllCategories();
        CategoryDto GetCategory(int id);
        void MoveUp(int categoryId);
        void MoveDown(int categoryId);
        void AddCategory(CategoryBaseDto category);
        void UpdateCategory(CategoryBaseDto category);
        void DeleteCategory(CategoryBaseDto category);
    }
}
