﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.DTO.Account;

namespace XnForum.Services.Contracts.Interfaces
{
    public interface IAccountService
    {
        UserDto GetUser(int id);
        UserDto GetUser(string login);
        UserSessionDataDto Login(UserLoginDto user);
        //UserSessionDataDto LoginByFacebook(string id, string name);
        bool Register(UserRegisterDto user);
        IList<UserDto> GetAllUsers();
        UserSettingsDto GetUserSettings(int id);
        void UpdateUserSettings(int id, UserSettingsDto model);
    }
}
