﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.DTO;

namespace XnForum.Services.Contracts.Interfaces
{
    public interface IForumService
    {
        IList<ForumBaseDto> GetAllForums();
        AllForumDataDto GetAllForumData();
        ForumDto GetForum(int id, int page);
        ForumDto GetForum(int id);
        ForumBaseDto GetForumBase(int id);
        void AddForum(ForumBaseDto forum);
        void UpdateForum(ForumBaseDto forum);
        void DeleteForum(ForumBaseDto forum);
    }
}
