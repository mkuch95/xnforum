﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.DTO;

namespace XnForum.Services.Contracts.Interfaces
{
    public interface IThreadService
    {
        ThreadDisplayDto GetThread(int id, int page);
        ThreadDisplayDto GetThread(int id);
        int AddThread(ThreadCreateDto thread);
        ThreadCreateDto GetNewThread();
        bool AddPost(PostCreateDto post);
    }
}
