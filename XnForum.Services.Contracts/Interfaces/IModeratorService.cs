﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.DTO.Moderator;
using XnForum.Services.Contracts.DTO.Report;

namespace XnForum.Services.Contracts.Interfaces
{
    public interface IModeratorService
    {
        ModeratorCreateDto GetNewModerator();
        ModeratorDisplayDto GetModerator(int id);
        IList<ModeratorDisplayDto> GetAllModerators();
        IList<ModeratorDisplayDto> GetForumModerators(int forumId);
        void AddModerator(ModeratorCreateDto moderator);
        void DeleteModerator(ModeratorDisplayDto moderator);

        ModeratorReportPageDto GetModeratorPage(int userId, int? forumId);
        IList<ForumBaseDto> GetModeratorForums(int userId);
        IList<ReportedPostDto> GetReportedPosts(int forumId);
        ReportedPostDto GetReportedPost(int userId, int postId);
        PostEditDto GetPostToEdit(int userId, int postId);
        void DeletePost(int userId, int postId);
        void IgnorePostReports(int userId, int postId);
        void EditPost(int userId, PostEditDto post);
    }
}
