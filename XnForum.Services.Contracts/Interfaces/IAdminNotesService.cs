﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.DTO;

namespace XnForum.Services.Contracts.Interfaces
{
    public interface IAdminNotesService
    {
        //TODO
        //moveUP
        //moveDown
        AdminNoteDto GetAdminNote(int id);
        IList<AdminNoteDto> GetAllVisibleNotes();
        IList<AdminNoteDto> GetAllNotes();
        void AddNote(AdminNoteDto adminNote);
        void UpdateNote(AdminNoteDto adminNote);
        void DeleteNote(AdminNoteDto adminNote);
    }
}
