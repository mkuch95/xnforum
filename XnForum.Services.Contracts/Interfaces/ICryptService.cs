﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Contracts.Interfaces
{
    public interface ICryptService
    {
        bool IsValid(string password, string salt, string passwordHash);
        string GetRandomSalt();
        string HashPassword(string password, string salt);
    }
}
