﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.DTO.Search;

namespace XnForum.Services.Contracts.Interfaces
{
    public interface ISearchService
    {
        SearchFormDto GetSearchForm();
        SearchResultDto Search(SearchFormDto data);
    }
}
