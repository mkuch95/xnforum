﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.DTO;

namespace XnForum.Services.Contracts.Interfaces
{
    public interface IReportService
    {
        void AddReportReason(ReportReasonDto reportReason);
        void DeleteReportReason(ReportReasonDto reportReason);
        void EditReportReason(ReportReasonDto reportReason);
        ReportReasonDto GetReportReason(int id);
        IList<ReportReasonDto> GetAllReportReasons();
        void ReportPost(int userId, int postId, int reasonId);
    }
}
