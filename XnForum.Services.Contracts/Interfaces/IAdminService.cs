﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Contracts.Interfaces
{
    public interface IAdminService
    {
        IList<string> GetAllBadWords();
        void AddBadWord(string text);
        void DeleteBadWord(string text);

        IList<string> GetAllHtmlTags();
        void AddHtmlTag(string tag);
        void DeleteHtmlTag(string tag);
    }
}
