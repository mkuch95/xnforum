﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Contracts.Enums
{
    public enum SentMessageStatus
    {
        New,
        Deleted
    }

    public enum ReceivedMessageStatus
    {
        New,
        Readed,
        Deleted
    }
}
