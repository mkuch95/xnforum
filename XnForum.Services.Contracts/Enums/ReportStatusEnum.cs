﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Contracts.Enums
{
    public enum ReportStatusEnum
    {
        Unverified = 0,
        Ignored,
        Confirmed
    }
}
