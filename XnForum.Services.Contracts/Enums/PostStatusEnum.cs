﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Contracts.Enums
{
    public enum PostStatusEnum
    {
        Ok = 0,
        EditedByModerator,
        DeletedByModerator,
        DeletedByUser
    }
}
