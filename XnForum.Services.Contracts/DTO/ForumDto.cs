﻿using System.Collections.Generic;

namespace XnForum.Services.Contracts.DTO
{
    public class ForumDto : ForumBaseDto
    {
        public ForumDto()
        {
            Threads = new List<ThreadBaseDto>();
        }

        public int Page { get; set; }
        public int PagesCount { get; set; }

        public IList<ThreadBaseDto> Threads { get; set; }
        //public IList<ModeratorDto> Moderators { get; set; }
    }
}
