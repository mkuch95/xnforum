﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.DTO.Message;

namespace XnForum.Services.Contracts.DTO
{
    public class MessageTalkDto
    {
        public MessageTalkDto()
        {
            Messages = new List<MessageDisplayDto>();
            MessageCreate = new MessageCreateDto();
        }

        public IList<MessageDisplayDto> Messages { get; set; }

        public MessageCreateDto MessageCreate { get; set; }

        public string UserLogin { get; set; }
        public int Page { get; set; }
        public int PagesCount { get; set; }

    }
}
