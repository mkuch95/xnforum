﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Contracts.DTO.Message
{
    public class TalksListDto
    {
        public TalksListDto()
        {
            Messages = new List<MessageHeaderDto>();
        }

        public IList<MessageHeaderDto> Messages { get; set; }
        public int Page { get; set; }
        public int PagesCount { get; set; }
    }
}
