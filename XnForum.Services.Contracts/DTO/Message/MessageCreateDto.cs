﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Contracts.DTO.Message
{
    public class MessageCreateDto
    {
        public int FromUserId { get; set; }
        public string ToUserLogin { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Treść wiadomości")]
        public string Text { get; set; }
    }
}
