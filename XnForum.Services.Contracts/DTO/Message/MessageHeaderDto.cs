﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Contracts.DTO
{
    public class MessageHeaderDto
    {
        public int UserId { get; set; }
        [Display(Name = "Rozmówca")]
        public string UserLogin { get; set; }
        [Display(Name = "Data")]
        public DateTime Date { get; set; }
        [Display(Name = "Ostatnia wiadomość")]
        public string ShortText { get; set; }
        public bool New { get; set; }
    }
}
