﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Contracts.DTO
{
    public class MessageDisplayDto
    {
        public int Id { get; set; }
        public int ToUserId { get; set; }
        public string ToUserLogin { get; set; }
        public int FromUserId { get; set; }
        public string FromUserLogin { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }
        public bool New { get; set; }
    }
}
