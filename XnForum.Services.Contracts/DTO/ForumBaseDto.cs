﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace XnForum.Services.Contracts.DTO
{
    public class ForumBaseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Display(Name = "Anonimowi mogą dodawać posty")]
        public bool AnonymousPosts { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
