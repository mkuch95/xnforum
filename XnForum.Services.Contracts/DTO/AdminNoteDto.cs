﻿using System.Runtime.Serialization;

namespace XnForum.Services.Contracts.DTO
{
    public class AdminNoteDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}
