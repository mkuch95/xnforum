﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace XnForum.Services.Contracts.DTO
{
    public class ReportReasonDto
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Powód")]
        public string Text { get; set; }
    }
}
