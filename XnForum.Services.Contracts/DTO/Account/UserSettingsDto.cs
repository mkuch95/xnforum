﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.Files;
using XnValidation.Validation;

namespace XnForum.Services.Contracts.DTO.Account
{
    public class UserSettingsDto : IValidationModel
    {
        [DisplayName("Hasło")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DisplayName("Powtórz hasło")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Hasła muszą być identyczne!")]
        public string RePassword { get; set; }

        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DisplayName("Aktualny awatar")]
        public string Avatar { get; set; }

        [DisplayName("Awatar")]
        public IInputFile AvatarFile { get; set; }

        public IValidationDictionary ModelState { get; set; }
    }
}
