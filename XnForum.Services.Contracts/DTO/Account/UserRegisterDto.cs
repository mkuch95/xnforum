﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnValidation.Validation;

namespace XnForum.Services.Contracts.DTO
{
    public class UserRegisterDto : IValidationModel
    {
        [Required]
        [RegularExpression(@"^[a-zA-Z0-9_]+$", ErrorMessage = "Niedozwolone znaki")]
        [DisplayName("Login")]
        public string Login { get; set; }
        [Required]
        [DisplayName("Hasło")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [DisplayName("Powtórz hasło")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Hasła muszą być identyczne!")]
        public string RePassword { get; set; }
        [Required]
        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public IValidationDictionary ModelState { get; set; }
    }
}
