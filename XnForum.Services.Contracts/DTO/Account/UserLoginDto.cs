﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using XnValidation.Validation;

namespace XnForum.Services.Contracts.DTO.Account
{
    public class UserLoginDto : IValidationModel
    {
        [Required]
        [DisplayName("Login")]
        public string Login { get; set; }
        [Required]
        [DisplayName("Hasło")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public IValidationDictionary ModelState { get; set; }
    }
}
