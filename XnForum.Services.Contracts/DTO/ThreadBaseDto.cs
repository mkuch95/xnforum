﻿
namespace XnForum.Services.Contracts.DTO
{
    public class ThreadBaseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Views { get; set; }
        public int UserId { get; set; }
        public string UserLogin { get; set; }
    }
}
