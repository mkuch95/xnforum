﻿namespace XnForum.Services.Contracts.DTO
{
    public class PostFileDto
    {
        public int Id { get; set; }
        public string FileName { get; set; }
    }
}
