﻿namespace XnForum.Services.Contracts.DTO
{
    public class ModeratorDto
    {
        public int Id { get; set; }
        public UserDto User { get; set; }
        public ForumDto Forum { get; set; }
    }
}
