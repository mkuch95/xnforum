﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.Files;
using XnValidation.Validation;

namespace XnForum.Services.Contracts.DTO
{
    public class PostEditDto : IValidationModel
    { 

        public int Id { get; set; }
        public int? UserId { get; set; }
        public int ThreadId { get; set; }

        [Required]
        [Display(Name = "Treść")]
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }

        public IValidationDictionary ModelState { get; set; }
        
    }
}
