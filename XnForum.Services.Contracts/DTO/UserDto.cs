﻿using System.Collections.Generic;
using System.ComponentModel;

namespace XnForum.Services.Contracts.DTO
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Login { get; set; }
        [DisplayName("Awatar")]
        public string Avatar { get; set; }
    }
}
