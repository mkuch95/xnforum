﻿using System.Collections.Generic;

namespace XnForum.Services.Contracts.DTO
{
    public class CategoryBaseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
