﻿using XnForum.Services.Contracts.Enums;

namespace XnForum.Services.Contracts.DTO
{
    public class ReportDto
    {
        public int Id { get; set; }
        public string UserLogin { get; set; }
        public int UserId { get; set; }

        public ReportReasonDto Reason { get; set; }
        public int PostId { get; set; }
        public ReportStatusEnum Status { get; set; }
    }
}
