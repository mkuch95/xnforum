﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Contracts.DTO.Report
{
    public class ReportedPostDto
    {
        public PostDisplayDto Post { get; set; }
        public IList<ReportDto> Reports { get; set; }
    }
}
