﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Contracts.DTO
{
    public class AllForumDataDto
    {
        public IList<AdminNoteDto> AdminNotes { get; set; }

        public IList<CategoryDto> Categories { get; set; }
    }
}
