﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using XnForum.Services.Contracts.Files;
using XnValidation.Validation;

namespace XnForum.Services.Contracts.DTO
{
    public class PostCreateDto : IValidationModel
    {
        public PostCreateDto()
        {
            Files = new List<IInputFile>();
        }

        public int? UserId { get; set; }
        public int ThreadId { get; set; }

        [Required]
        [Display(Name = "Treść")]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Text { get; set; }

        public IEnumerable<IInputFile> Files { get; set; }
        public IValidationDictionary ModelState { get; set; }
    }
}
