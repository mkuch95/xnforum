﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.Enums;

namespace XnForum.Services.Contracts.DTO
{
    public class ThreadDisplayDto
    {
        public ThreadDisplayDto()
        {
            Posts = new List<PostDisplayDto>();
            PostCreate = new PostCreateDto();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Views { get; set; }
        public bool ForumAnonymousPosts { get; set; }

        public PostDisplayDto Post { get; set; }
        
        public int ForumId { get; set; }
        public string ForumName { get; set; }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public IList<PostDisplayDto> Posts { get; set; }

        public PostCreateDto PostCreate { get; set; }

        public int Page { get; set; }
        public int PagesCount { get; set; }
    }
}
