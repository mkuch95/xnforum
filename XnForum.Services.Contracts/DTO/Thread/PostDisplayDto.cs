﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.Enums;

namespace XnForum.Services.Contracts.DTO
{
    public class PostDisplayDto
    {
        public PostDisplayDto()
        {
            Files = new List<PostFileDto>();
        }

        public int Id { get; set; }
        public int? UserId { get; set; }
        [DisplayName("Login")]
        public string UserLogin { get; set; }
        [DisplayName("Awatar")]
        public string UserAvatar { get; set; }
        [DisplayName("Data dodania")]
        public int ThreadId { get; set; }
        public DateTime Date { get; set; }
        [DisplayName("Treść")]
        public string Text { get; set; }
        public PostStatusEnum Status { get; set; }

        [DisplayName("Załączone pliki")]
        public virtual IList<PostFileDto> Files { get; set; }
    }
}
