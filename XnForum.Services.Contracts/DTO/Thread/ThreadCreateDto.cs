﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using XnForum.Services.Contracts.Files;
using XnValidation.Validation;

namespace XnForum.Services.Contracts.DTO
{
    public class ThreadCreateDto : IValidationModel
    {
        [Required]
        [Display(Name = "Tytuł")]
        public string Name { get; set; }
        public int UserId { get; set; }
        [Required]
        [Display(Name = "Forum")]
        public int ForumId { get; set; }
        public IList<ForumBaseDto> Forums { get; set; }

        [Required]
        [Display(Name = "Treść")]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Text { get; set; }

        [DisplayName("Załączniki")]
        public IEnumerable<IInputFile> Files { get; set; }

        public IValidationDictionary ModelState { get; set; }
    }
}
