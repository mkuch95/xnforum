﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Contracts.DTO.Moderator
{
    public class ModeratorDisplayDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        [Display(Name = "Login")]
        public string UserLogin { get; set; }
        public int ForumId { get; set; }
        [Display(Name = "Forum")]
        public string ForumName { get; set; }
        [Display(Name = "Data")]
        public DateTime Date { get; set; }
    }
}
