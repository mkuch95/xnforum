﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnValidation.Validation;

namespace XnForum.Services.Contracts.DTO.Moderator
{
    public class ModeratorCreateDto : IValidationModel
    {
        public ModeratorCreateDto()
        {
            Forums = new List<ForumBaseDto>();
            Users = new List<UserDto>();
        }

        public int Id { get; set; }

        [Required]
        [Display(Name = "Użytkownik")]
        public int UserId { get; set; }
        public IList<UserDto> Users { get; set; }
        [Required]
        [Display(Name = "Forum")]
        public int ForumId { get; set; }
        public IList<ForumBaseDto> Forums { get; set; }
        //public DateTime Date { get; set; }
        public IValidationDictionary ModelState { get; set; }
    }
}
