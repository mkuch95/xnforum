﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.DTO.Report;

namespace XnForum.Services.Contracts.DTO.Moderator
{
    public class ModeratorReportPageDto
    {
        public ModeratorReportPageDto()
        {
            Forums = new List<ForumBaseDto>();
            ReportedPosts = new List<ReportedPostDto>();
        }

        public IList<ForumBaseDto> Forums { get; set; }

        public IList<ReportedPostDto> ReportedPosts { get; set; }

        //todo page??
    }
}
