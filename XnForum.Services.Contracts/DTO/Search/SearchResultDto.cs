﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.Enums;

namespace XnForum.Services.Contracts.DTO.Search
{
    public class SearchResultDto : SearchFormDto
    {
        public SearchResultDto()
        {
            Posts = new List<PostDisplayDto>();
        }

        public IList<ThreadDisplayDto> Threads { get; set; }
        public IList<PostDisplayDto> Posts { get; set; }
    }
}
