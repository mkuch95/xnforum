﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.Enums;

namespace XnForum.Services.Contracts.DTO.Search
{
    public class SearchFormDto
    {
        public SearchFormDto()
        {
            Forums = new List<ForumBaseDto>();
        }
        [Required]
        [Display(Name = "Forum")]
        public int? ForumId { get; set; }
        public IList<ForumBaseDto> Forums { get; set; }
        public string Text { get; set; }
    }
}
