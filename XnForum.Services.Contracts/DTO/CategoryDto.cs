﻿using System.Collections.Generic;

namespace XnForum.Services.Contracts.DTO
{
    public class CategoryDto : CategoryBaseDto
    {
        public IList<ForumBaseDto> Forums { get; set; }
    }
}
