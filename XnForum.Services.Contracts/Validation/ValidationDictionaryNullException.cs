﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnValidation.Validation
{
    public class NullValidationDictionaryException : Exception
    {
        public NullValidationDictionaryException() : base("Validation Dictionary is not set!") { }
    }
}
