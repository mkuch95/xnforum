﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnValidation.Validation
{
    public static class ValidationExtension
    {
        public static void AddError(this IValidationModel @this, string key, string errorMessage)
        {
            if(@this.ModelState == null)
                throw new NullValidationDictionaryException();

            @this.ModelState.AddError(key, errorMessage);
        }
    }
}
