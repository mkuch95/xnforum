﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnValidation.Validation
{
    public interface IValidationModel
    {
        IValidationDictionary ModelState { get; set; }
    }
}
