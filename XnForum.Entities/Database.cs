﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System.IO;
using Repository.Pattern.Infrastructure;
using XnForum.Entities.Model.Mapping;

namespace XnForum.Entities
{
    public class Database : IDatabase
    {
        private static ISessionFactory _sessionFactory = null;
        private static object _lockObj = new object();

        public ISessionFactory SessionFactory
        {
            get
            {
                if (_sessionFactory == null)
                {
                    lock (_lockObj)
                    {
                        if (_sessionFactory == null)
                        {
                            _sessionFactory = Fluently.Configure()
                                .Database(MsSqlConfiguration.MsSql2008
                                        .ConnectionString(c => c.FromConnectionStringWithKey("DBConnection"))
                                )
                                .Mappings(m =>
                                        m.FluentMappings.AddFromAssemblyOf<AdminNoteMap>())
                                .Mappings(m =>
                                        m.FluentMappings.AddFromAssemblyOf<CategoryMap>())
                                .Mappings(m =>
                                        m.FluentMappings.AddFromAssemblyOf<ForumMap>())
                                .Mappings(m =>
                                        m.FluentMappings.AddFromAssemblyOf<MessageMap>())
                                .Mappings(m =>
                                        m.FluentMappings.AddFromAssemblyOf<ModeratorMap>())
                                .Mappings(m =>
                                        m.FluentMappings.AddFromAssemblyOf<PostFileMap>())
                                .Mappings(m =>
                                        m.FluentMappings.AddFromAssemblyOf<PostMap>())
                                .Mappings(m =>
                                        m.FluentMappings.AddFromAssemblyOf<ThreadMap>())
                                .Mappings(m =>
                                        m.FluentMappings.AddFromAssemblyOf<UserMap>())
                                .Mappings(m =>
                                        m.FluentMappings.AddFromAssemblyOf<ReportMap>())
                                .Mappings(m =>
                                        m.FluentMappings.AddFromAssemblyOf<ReportReasonMap>())
                                //
                                //
                                //
                                .ExposeConfiguration(TreatConfiguration)
                                //.CurrentSessionContext("web")
                                .BuildSessionFactory();
                        }
                    }
                }

                return _sessionFactory;
            }
        }

        private static void TreatConfiguration(Configuration configuration)
        {
            var update = new SchemaUpdate(configuration);
            //update.Execute(false, true);
            update.Execute(LogAutoMigration, true);
        }

        private static void LogAutoMigration(string sql)
        {
            using (var file = new FileStream(@"C:\update.sql", FileMode.Append))
            {
                using (var sw = new StreamWriter(file))
                {
                    sw.Write(sql);
                }
            }
        }

    }
}
