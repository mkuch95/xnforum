﻿using System;
using Repository.Pattern.Infrastructure;

namespace XnForum.Entities.Model
{
    public class Moderator : IEntity
    {
        public virtual int Id { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual User User { get; set; }
        public virtual Forum Forum { get; set; }
    }
}
