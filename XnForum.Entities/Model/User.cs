﻿using Repository.Pattern.Infrastructure;
using System.Collections.Generic;
using XnForum.Services.Contracts.Enums;

namespace XnForum.Entities.Model
{
    public class User : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Login { get; set; }
        public virtual string Password { get; set; }
        public virtual string Salt { get; set; }
        public virtual string Email { get; set; }
        public virtual string Avatar { get; set; }
        public virtual UserRoleEnum Role { get; set; }
        
        //public virtual int? FacebookId { get; set; }
        public virtual IList<Moderator> Moderators { get; set; }
        public virtual IList<Post> Posts { get; set; }
        public virtual IList<Message> Received { get; set; }
        public virtual IList<Message> Sent { get; set; }
    }
}
