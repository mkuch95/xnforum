﻿using Repository.Pattern.Infrastructure;

namespace XnForum.Entities.Model
{
    public class PostFile : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string FileName { get; set; }
        public virtual Post Post { get; set; }
    }
}
