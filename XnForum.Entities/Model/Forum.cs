﻿using Repository.Pattern.Infrastructure;
using System.Collections.Generic;

namespace XnForum.Entities.Model
{
    public class Forum : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual Category Category { get; set; }
        public virtual bool AnonymousPosts { get; set; }

        public virtual IList<Thread> Threads { get; set; }
        public virtual IList<Moderator> Moderators { get; set; }
    }
}
