﻿using Repository.Pattern.Infrastructure;
using System;
using System.Collections.Generic;
using XnForum.Services.Contracts.Enums;

namespace XnForum.Entities.Model
{
    public class Post : IEntity
    {
        public virtual int Id { get; set; }
        public virtual User User { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Text { get; set; }

        public virtual Thread ThreadPost { get; set; }
        public virtual Thread Thread { get; set; }
        public virtual PostStatusEnum Status { get; set; }

        public virtual IList<Report> Reports { get; set; }
        public virtual IList<PostFile> Files { get; set; }
    }
}
