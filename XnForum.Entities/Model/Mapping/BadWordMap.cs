﻿using FluentNHibernate.Mapping;

namespace XnForum.Entities.Model.Mapping
{
    public class BadWordMap : ClassMap<BadWord>
    {
        public BadWordMap()
        {
            Id(x => x.Id);

            Map(x => x.Value);

            Table("BadWords");
        }
    }
}
