﻿using FluentNHibernate.Mapping;

namespace XnForum.Entities.Model.Mapping
{
    public class ForumMap : ClassMap<Forum>
    {
        public ForumMap()
        {
            Id(x => x.Id);

            Map(x => x.Name);

            Map(x => x.AnonymousPosts);

            References(x => x.Category)
                .Column("CategoryId");

            HasMany(x => x.Threads)
                .KeyColumn("ForumId")
                .Inverse()
                .Cascade
                .AllDeleteOrphan();

            HasMany(x => x.Moderators)
                .KeyColumn("ForumId")
                .Inverse()
                .Cascade
                .AllDeleteOrphan();

            Table("Forums");
        }
    }
}
