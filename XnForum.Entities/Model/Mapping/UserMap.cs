﻿using FluentNHibernate.Mapping;
using XnForum.Services.Contracts.Enums;

namespace XnForum.Entities.Model.Mapping
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Id(x => x.Id);

            Map(x => x.Login)
                .Index("idx_Login");

            Map(x => x.Password);

            Map(x => x.Salt);

            Map(x => x.Email);

            Map(x => x.Role);
            //  .CustomType<UserRoleEnum>();

            Map(x => x.Avatar);

            //Map(x => x.FacebookId);

            HasMany(x => x.Moderators)
                .KeyColumn("UserId")
                .Inverse()
                .Cascade
                .All();

            HasMany(x => x.Posts)
                .KeyColumn("UserId")
                .Inverse()
                .Cascade
                .All();

            HasMany(x => x.Received)
                .KeyColumn("ToUserId")
                .Inverse()
                .Cascade
                .All();

            HasMany(x => x.Sent)
                .KeyColumn("FromUserId")
                .Inverse()
                .Cascade
                .All();

            Table("Users");
        }
    }
}
