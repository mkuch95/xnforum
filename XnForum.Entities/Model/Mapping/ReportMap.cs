﻿using FluentNHibernate.Mapping;
using XnForum.Services.Contracts.Enums;

namespace XnForum.Entities.Model.Mapping
{
    public class ReportMap : ClassMap<Report>
    {
        public ReportMap()
        {
            Id(x => x.Id);

            References(x => x.User)
                .Column("UserId");

            References(x => x.Reason)
                .Column("ReportReasonId");

            References(x => x.Post)
                .Column("PostId");

            Map(x => x.Status)
                .CustomType<ReportStatusEnum>();
            
            Table("Reports");
        }
    }
}
