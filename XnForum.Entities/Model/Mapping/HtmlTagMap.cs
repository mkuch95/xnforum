﻿using FluentNHibernate.Mapping;

namespace XnForum.Entities.Model.Mapping
{
    public class HtmlTagMap : ClassMap<HtmlTag>
    {
        public HtmlTagMap()
        {
            Id(x => x.Id);

            Map(x => x.Tag);

            Table("HtmlTags");
        }
    }
}
