﻿using FluentNHibernate.Mapping;
using XnForum.Services.Contracts.Enums;

namespace XnForum.Entities.Model.Mapping
{
    public class PostMap : ClassMap<Post>
    {
        public PostMap()
        {
            Id(x => x.Id);

            References(x => x.User)
                .Column("UserId");

            Map(x => x.Date);

            Map(x => x.Text);

            Map(x => x.Status)
                .CustomType<PostStatusEnum>();

            HasOne(x => x.ThreadPost)
                .PropertyRef(x => x.Post);
                //.KeyColumn("ThreadId")
                //.Inverse()
                //.Cascade
                //.AllDeleteOrphan();

            References(x => x.Thread)
                .Column("ThreadId");

            HasMany(x => x.Reports)
                .KeyColumn("PostId")
                .Inverse()
                .Cascade
                .AllDeleteOrphan();

            HasMany(x => x.Files)
                .KeyColumn("PostId")
                .Inverse()
                .Cascade
                .AllDeleteOrphan();

            Table("Posts");
        }
    }
}
