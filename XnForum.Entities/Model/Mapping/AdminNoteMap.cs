﻿using FluentNHibernate.Mapping;

namespace XnForum.Entities.Model.Mapping
{
    public class AdminNoteMap : ClassMap<AdminNote>
    {
        public AdminNoteMap()
        {
            Id(x => x.Id);

            Map(x => x.Text);

            Table("AdminNotes");
        }
    }
}
