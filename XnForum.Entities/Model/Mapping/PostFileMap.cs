﻿using FluentNHibernate.Mapping;

namespace XnForum.Entities.Model.Mapping
{
    public class PostFileMap : ClassMap<PostFile>
    {
        public PostFileMap()
        {
            Id(x => x.Id);

            Map(x => x.FileName);

            References(x => x.Post)
                .Column("PostId");

            Table("PostFiles");
        }
    }
}
