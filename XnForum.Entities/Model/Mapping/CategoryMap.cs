﻿using FluentNHibernate.Mapping;

namespace XnForum.Entities.Model.Mapping
{
    public class CategoryMap : ClassMap<Category>
    {
        public CategoryMap()
        {
            Id(x => x.Id);

            Map(x => x.Name);

            Map(x => x.Position)
                .Not.Nullable();

            HasMany(x => x.Forums)
                .KeyColumn("CategoryId")
                .Inverse()
                .Cascade
                .AllDeleteOrphan();

            Table("Categories");
        }
    }
}
