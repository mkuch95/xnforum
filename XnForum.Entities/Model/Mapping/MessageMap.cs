﻿using System.Diagnostics.Contracts;
using FluentNHibernate.Mapping;
using XnForum.Services.Contracts.Enums;

namespace XnForum.Entities.Model.Mapping
{
    public class MessageMap : ClassMap<Message>
    {
        public MessageMap()
        {
            Id(x => x.Id);

            References(x => x.From)
                .Column("FromUserId");

            References(x => x.To)
                .Column("ToUserId");

            Map(x => x.Date);

            Map(x => x.ReceivedStatus)
                .CustomType<ReceivedMessageStatus>();

            Map(x => x.SentStatus)
                .CustomType<SentMessageStatus>();

            Map(x => x.Text);

            Table("Messages");
        }
    }
}
