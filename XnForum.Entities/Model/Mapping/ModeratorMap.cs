﻿using FluentNHibernate.Mapping;

namespace XnForum.Entities.Model.Mapping
{
    public class ModeratorMap : ClassMap<Moderator>
    {
        public ModeratorMap()
        {
            Id(x => x.Id);

            Map(x => x.Date);

            References(x => x.User)
                .Column("UserId");

            References(x => x.Forum)
                .Column("ForumId");

            Table("Moderators");
        }
    }
}
