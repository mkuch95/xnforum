﻿using FluentNHibernate.Mapping;

namespace XnForum.Entities.Model.Mapping
{
    public class ReportReasonMap : ClassMap<ReportReason>
    {
        public ReportReasonMap()
        {
            Id(x => x.Id);

            Map(x => x.Text);

            Map(x => x.Deleted);

            HasMany(x => x.Reports)
                .KeyColumn("ReportReasonId")
                .Inverse()
                .Cascade
                .All();

            Table("ReportReasons");
        }
    }
}
