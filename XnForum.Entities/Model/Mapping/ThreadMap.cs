﻿using FluentNHibernate.Mapping;

namespace XnForum.Entities.Model.Mapping
{
    public class ThreadMap : ClassMap<Thread>
    {
        public ThreadMap()
        {
            Id(x => x.Id);

            Map(x => x.Name);

            Map(x => x.Views);

            References(x => x.Forum)
                .Column("ForumId");
            
            References(x => x.Post)//FIXME delete cascade
                .Column("PostId")
                .Not.Nullable();
                
            HasMany(x=>x.Posts)
                .KeyColumn("ThreadId")
                .Inverse()
                .Cascade
                .AllDeleteOrphan();

            Table("Threads");
        }
    }
}
