﻿using Repository.Pattern.Infrastructure;
using XnForum.Services.Contracts.Enums;

namespace XnForum.Entities.Model
{
    public class Report : IEntity
    {
        public virtual int Id { get; set; }
        public virtual User User { get; set; }
        public virtual ReportReason Reason { get; set; }
        public virtual Post Post { get; set; }
        public virtual ReportStatusEnum Status { get; set; }
    }
}
