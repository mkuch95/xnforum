﻿using Repository.Pattern.Infrastructure;

namespace XnForum.Entities.Model
{
    public class AdminNote : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Text { get; set; }
    }
}
