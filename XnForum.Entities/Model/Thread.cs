﻿using System.Collections.Generic;
using Repository.Pattern.Infrastructure;

namespace XnForum.Entities.Model
{
    public class Thread : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int Views { get; set; }
        public virtual Forum Forum { get; set; }
        public virtual Post Post { get; set; }

        public virtual IList<Post> Posts { get; set; }
    }
}
