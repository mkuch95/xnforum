﻿using Repository.Pattern.Infrastructure;
using System;
using XnForum.Services.Contracts.Enums;

namespace XnForum.Entities.Model
{
    public class Message : IEntity
    {
        public virtual int Id { get; set; }
        public virtual User From { get; set; }
        public virtual User To { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual ReceivedMessageStatus ReceivedStatus { get; set; }
        public virtual SentMessageStatus SentStatus { get; set; }
        public virtual string Text { get; set; }
    }
}
