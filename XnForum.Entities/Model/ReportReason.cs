﻿using Repository.Pattern.Infrastructure;
using System.Collections.Generic;

namespace XnForum.Entities.Model
{
    public class ReportReason : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Text { get; set; }
        public virtual bool Deleted { get; set; }
        public virtual IList<Report> Reports { get; set; }
    }
}
