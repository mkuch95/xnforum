﻿using System.Collections.Generic;
using Repository.Pattern.Infrastructure;

namespace XnForum.Entities.Model
{
    public class Category : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int Position { get; set; }

        public virtual IList<Forum> Forums { get; set; }
    }
}
