Projekt zaliczający przedmiot Aplikacje webowe w ASP MVC.  
  
**Opis projektu**  
Celem projektu było stworzenie forum internetowego forum dyskusyjnego. Za jego pomocą internauci mogą komunikować się i wspólnie rozmawiać na różne tematy.   
  
**Użyte techonologie**  
ASP.NET MVC 5  
nHibenrate + Fluent NHibernate  
baza MSSQL lokalna, może być dowolna wspierana przez nHibernate  
AutoMapper  
Log4Net + Log2Console do podglądu wygenerowanych zapytań SQL  
Autofac  
Owin(w planach było logowanie przez Facebooka)  


**Opis funkcjonalności**   
- panel administratora   
- forum z możliwością tworzenia wątków i dopisywania wiadomości do istniejących wątków   
- rejestracja użytkowników. Profil użytkownika. Możliwość zalogowania się do systemu   
- możliwość definiowania kategorii forów i samych forów. Funkcjonalność dostępna tylko dla administratora. Dane forum może należeć do dokładnie jednej kategorii   
- uprawnienia dla użytkowników anonimowych przydzielone poszczególnym forom. Istnieją dwa poziomy uprawnień: do oglądania wiadomości i do pisania własnych. Użytkownicy zalogowani mają pełne uprawnienia do oglądania i pisania    
- przydzielenie moderatorów dla każdego forum. Każde forum może mieć wielu moderatorów a każdy użytkownik może być moderatorem na wielu forach. Moderator ma prawo edycji i usuwania wiadomości    
- ogłoszenia administracyjne wyświetlane na stronie głównej forum   
- wysyłanie wiadomości prywatnych pomiędzy użytkownikami. Możliwość odczytu i usunięcia wiadomości po zalogowaniu   
- możliwość zgłoszenia wiadomości do moderacji. Zgłoszenie pokazuje się jako wiadomość u moderatora    
- słownik słów zakazanych. Automatyczne nieprzyjmowanie wiadomości zawierających  takie słowa. Zarządzanie słownikiem przez administratora   
- dołączanie załączników do wiadomości. Ograniczenie liczby załączników i wielkości pojedynczego załącznika   
- emotikony w treściach wiadomości   
- awatary w profilu użytkownika. Ograniczenie na rozdzielczość i wielkość (w kB) wgrywanych obrazków   
- możliwość   umieszczania   wiadomości   w   formacie HTML. Definiowanie przez administratora listy dozwolonych znaczników. Automatyczne usuwanie znaczników spoza listy   
- wyszukiwanie według słów kluczowych w treściach wiadomości z danego forum   
- operatory typu and, or i not przy wyszukiwaniu  
- stronicowanie list wiadomości