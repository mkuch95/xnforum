﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using XnForum.Entities.Model;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.DTO.Account;
using XnForum.Services.Contracts.DTO.Moderator;
using XnForum.Services.Contracts.Enums;

namespace XnForum.UI.Web
{
    public class OrganizationProfile : Profile
    {
        public OrganizationProfile()
        {
            CreateMap<AdminNote, AdminNoteDto>();
            CreateMap<Category, CategoryBaseDto>();
            CreateMap<Forum, ForumDto>();

            CreateMap<Moderator, ModeratorDisplayDto>();
            CreateMap<ModeratorCreateDto, ModeratorCreateDto>();

            CreateMap<PostFile, PostFileDto>();
            CreateMap<Report, ReportDto>();
            CreateMap<ReportReason, ReportReasonDto>();
            CreateMap<User, UserDto>()
                .ForMember(x => x.Avatar, opt => opt.NullSubstitute("default.png"));
            CreateMap<User, UserSettingsDto>()
                .ForMember(x => x.Avatar, opt => opt.NullSubstitute("default.png"))
                .ForMember(x => x.Password, opt => opt.Ignore());

            CreateMap<Message, MessageDisplayDto>()
                .ForMember(x => x.FromUserId, opt => opt.MapFrom(x => x.From.Id))
                .ForMember(x => x.FromUserLogin, opt => opt.MapFrom(x => x.From.Login))
                .ForMember(x => x.ToUserId, opt => opt.MapFrom(x => x.To.Id))
                .ForMember(x => x.ToUserLogin, opt => opt.MapFrom(x => x.To.Login))
                .ForMember(x => x.New, opt => opt.MapFrom(x => x.ReceivedStatus == ReceivedMessageStatus.New));
            CreateMap<IList<Message>, MessageTalkDto>()
                .ForMember(x => x.Messages, opt => opt.MapFrom(x => x));
            CreateMap<Message, MessageHeaderDto>();

            CreateMap<User, UserSessionDataDto>()
                .ForMember(x => x.Avatar, opt => opt.NullSubstitute("default.png"));

            CreateMap<Post, PostDisplayDto>()
                .ForMember(x => x.UserAvatar, opt => opt.NullSubstitute("default.png"));
            CreateMap<Post, PostEditDto>();

            CreateMap<Thread, PostCreateDto>()
                .ForMember(x => x.ThreadId, opt => opt.MapFrom(x => x.Id));
            CreateMap<Thread, ThreadDisplayDto>()
                .ForMember(x => x.Posts, opt => opt.Ignore())
                .ForMember(x => x.PostCreate, opt => opt.MapFrom(x => x))
                .ForMember(x => x.CategoryId, opt => opt.MapFrom(x => x.Forum.Category.Id))
                .ForMember(x => x.CategoryName, opt => opt.MapFrom(x => x.Forum.Category.Name));


                //.ForMember(x => x.PostCreate.ThreadId, opt => opt.MapFrom(x => x.Id));
            CreateMap<ThreadCreateDto, ThreadCreateDto>();
            //
            CreateMap<Category, CategoryDto>();
            CreateMap<Forum, ForumBaseDto>();
            CreateMap<Thread, ThreadBaseDto>();


            
            // Use CreateMap... Etc.. here (Profile methods are the same as configuration methods)
        }

    }
}