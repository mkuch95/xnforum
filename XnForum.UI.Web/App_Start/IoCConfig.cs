﻿using Autofac;
using Autofac.Integration.Mvc;
using Repository.Pattern.NH;
using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Autofac.Integration.WebApi;
using Repository.Pattern.Infrastructure;
using XnForum.Services;
using XnForum.Services.Contracts.Interfaces;
using XnForum.Entities;
using XnForum.UI.Web.Controllers;

namespace XnForum.UI.Web
{
    public class IoCConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;


            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterWebApiFilterProvider(config);

            // You can register controllers all at once using assembly scanning...
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // ...or you can register individual controlllers manually.
            //builder.RegisterControllers(typeof(MvcApplication).Assembly);

            //uow
            builder.RegisterType<NHiberanteUnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<NHiberanteUnitOfWork>().As<INHiberanteUnitOfWork>().InstancePerRequest();
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>)).InstancePerRequest();
            builder.RegisterType<Database>().As<IDatabase>().SingleInstance();//FIXME

            // services
            builder.RegisterType<ForumService>().As<IForumService>().InstancePerRequest();
            builder.RegisterType<ThreadService>().As<IThreadService>().InstancePerRequest();
            builder.RegisterType<AdminNotesService>().As<IAdminNotesService>().InstancePerRequest();
            builder.RegisterType<CategoryService>().As<ICategoryService>().InstancePerRequest();
            builder.RegisterType<AccountService>().As<IAccountService>().InstancePerRequest();
            builder.RegisterType<AdminService>().As<IAdminService>().InstancePerRequest();
            builder.RegisterType<MessageService>().As<IMessageService>().InstancePerRequest();
            builder.RegisterType<ReportService>().As<IReportService>().InstancePerRequest();
            builder.RegisterType<ModeratorService>().As<IModeratorService>().InstancePerRequest();
            builder.RegisterType<CryptService>().As<ICryptService>().InstancePerRequest();
            builder.RegisterType<SearchService>().As<ISearchService>().InstancePerRequest();
            //

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            //GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);//FIXME
        }
    }
}
