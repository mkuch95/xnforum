﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace XnForum.UI.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            #region threads

            routes.MapRoute(
                name: "Thread + Pages",
                url: "Thread/{id}/page/{page}",
                defaults: new { controller = "Thread", action = "Thread"},
                namespaces: new[] { "XnForum.UI.Web.Controllers" }
            );

            routes.MapRoute(
                name: "Thread",
                url: "Thread/{id}",
                defaults: new { controller = "Thread", action="Thread", page = 1},
                namespaces: new[] { "XnForum.UI.Web.Controllers" }
            );

            #endregion

            #region forums

            routes.MapRoute(
                name: "Forum + Pages",
                url: "Forum/{id}/page/{page}",
                defaults: new { controller = "Forum", action = "Forum" },
                namespaces: new[] { "XnForum.UI.Web.Controllers" }
            );

            routes.MapRoute(
                name: "Forum",
                url: "Forum/{id}",
                defaults: new { controller = "Forum", action = "Forum", page = 1 },
                namespaces: new[] { "XnForum.UI.Web.Controllers" }
            );

            #endregion

            #region accounts

            routes.MapRoute(
                name: "UserAccount",
                url: "User/{login}",
                defaults: new { controller = "Account", action = "Account" },
                namespaces: new[] { "XnForum.UI.Web.Controllers" }
            );

            #endregion

            #region messages

            routes.MapRoute(
                name: "Messages + Pages",
                url: "Messages/page/{page}",
                defaults: new { controller = "Message", action = "Messages" },
                namespaces: new[] { "XnForum.UI.Web.Controllers" }
            );

            routes.MapRoute(
                name: "Messages",
                url: "Messages/",
                defaults: new { controller = "Message", action = "Messages", page = 1 },
                namespaces: new[] { "XnForum.UI.Web.Controllers" }
            );

            routes.MapRoute(
                name: "Talk + Pages",
                url: "Talk/{login}/page/{page}",
                defaults: new { controller = "Message", action = "Talk" },
                namespaces: new[] { "XnForum.UI.Web.Controllers" }
            );

            routes.MapRoute(
                name: "Talk",
                url: "Talk/{login}",
                defaults: new { controller = "Message", action = "Talk", page = 1 },
                namespaces: new[] { "XnForum.UI.Web.Controllers" }
            );
            //TODO group chats

            #endregion

            #region moderator

            routes.MapRoute(
                name: "moderator",
                url: "Moderator/",
                defaults: new { controller = "Moderator", action = "Reports" },
                namespaces: new[] { "XnForum.UI.Web.Controllers" }
            );

            routes.MapRoute(
                name: "moderator + forumid",
                url: "Moderator/Forum/{id}",
                defaults: new { controller = "Moderator", action = "Reports" },
                namespaces: new[] { "XnForum.UI.Web.Controllers" }
            );

            #endregion


            routes.MapHttpRoute(
                name: "DefaultApi1",
                routeTemplate: "api/{controller}/{id}/{page}",
                defaults: new { }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new {controller = "Forum", action = "Index", id = UrlParameter.Optional},
                namespaces: new[] {"XnForum.UI.Web.Controllers"}
            );


        }
        
    }
}
