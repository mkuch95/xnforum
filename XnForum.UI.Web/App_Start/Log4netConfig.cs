﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XnForum.UI.Web
{
    public class Log4netConfig
    {
        public static void Config()
        {
            log4net.Config.XmlConfigurator.Configure();
        }
    }
}