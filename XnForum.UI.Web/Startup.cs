﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Owin;

[assembly: OwinStartup(typeof(XnForum.UI.Web.Startup))]

namespace XnForum.UI.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        private void ConfigureAuth(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login")
            });

            /*
            app.UseFacebookAuthentication(new FacebookAuthenticationOptions
            {
                AppId = "xxx",//deleted
                AppSecret = "xxx",
                SignInAsAuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                Provider = new FacebookAuthenticationProvider
                {
                    OnAuthenticated = async ctx => {
                        
                    }
                }
            });
            */
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
        }
    }
}
