﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using XnForum.Services.Contracts.Files;

namespace XnForum.UI.Web.Files
{
    public class HttpInputFile : IInputFile
    {
        private HttpPostedFileBase file;

        public HttpInputFile(HttpPostedFileBase file)
        {
            this.file = file;
        }

        public int ContentLength
        {
            get { return file.ContentLength; }
        }

        public string ContentType
        {
            get { return file.ContentType; }
        }

        public string FileName
        {
            get { return file.FileName; }
        }

        public Stream InputStream
        {
            get { return file.InputStream; }
        }
    }
}