﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XnForum.UI.Web.Files
{
    public static class HttpInputFileExtension
    {
        public static IEnumerable<HttpInputFile> ToHttpInputFiles(this IEnumerable<HttpPostedFileBase> @this)
        {
            return @this
                .Where(x => x != null)
                .Select(x => new HttpInputFile(x))
                .ToList();
        }

        public static HttpInputFile ToHttpInputFile(this HttpPostedFileBase @this)
        {
            return new HttpInputFile(@this);
        }
    }
}