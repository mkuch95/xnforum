﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Interfaces;

namespace XnForum.UI.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ReportReasonController : Controller
    {
        private IReportService _reportService;

        public ReportReasonController(IReportService reportService)
        {
            _reportService = reportService;
        }


        // GET: Admin/ReportReason
        public ActionResult Index()
        {
            var model = _reportService.GetAllReportReasons();

            return View(model);
        }

        // GET: Admin/ReportReason/Details/5
        public ActionResult Details(int id)
        {
            var model = _reportService.GetReportReason(id);

            return View(model);
        }

        // GET: Admin/ReportReason/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/ReportReason/Create
        [HttpPost]
        public ActionResult Create(ReportReasonDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _reportService.AddReportReason(model);

            return RedirectToAction("Index");
        }

        // GET: Admin/ReportReason/Edit/5
        public ActionResult Edit(int id)
        {
            var model = _reportService.GetReportReason(id);

            return View(model);
        }

        // POST: Admin/ReportReason/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ReportReasonDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _reportService.EditReportReason(model);

            return RedirectToAction("Details", new {id = id});
        }

        // GET: Admin/ReportReason/Delete/5
        public ActionResult Delete(int id)
        {
            var model = _reportService.GetReportReason(id);

            return View(model);
        }

        // POST: Admin/ReportReason/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, ReportReasonDto model)
        {
            _reportService.DeleteReportReason(model);

            return RedirectToAction("Index");
        }
    }
}
