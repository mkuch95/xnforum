﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XnForum.Services.Contracts.Interfaces;

namespace XnForum.UI.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BadWordsController : Controller
    {
        private IAdminService _adminService;

        public BadWordsController(IAdminService adminService)
        {
            _adminService = adminService;
        }
        
        public ActionResult Index()
        {
            return View(_adminService.GetAllBadWords());
        }

        [HttpPost]
        public ActionResult Add(string text)
        {
            _adminService.AddBadWord(text);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(string text)
        {
            _adminService.DeleteBadWord(text);
            return RedirectToAction("Index");
        }
    }
}
