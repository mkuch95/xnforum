﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XnForum.Services.Contracts.Interfaces;

namespace XnForum.UI.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TagsController : Controller
    {
        private IAdminService _adminService;

        public TagsController(IAdminService adminService)
        {
            _adminService = adminService;
        }
        
        public ActionResult Index()
        {
            return View(_adminService.GetAllHtmlTags());
        }

        [HttpPost]
        public ActionResult Add(string tag)
        {
            _adminService.AddHtmlTag(tag);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(string tag)
        {
            _adminService.DeleteHtmlTag(tag);
            return RedirectToAction("Index");
        }
    }
}
