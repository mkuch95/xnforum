﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Interfaces;

namespace XnForum.UI.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminNotesController : Controller
    {
        private IAdminNotesService _adminNotesService;

        public AdminNotesController(IAdminNotesService adminNotesService)
        {
            _adminNotesService = adminNotesService;
        }

        // GET: AdminNotes
        public ActionResult Index()
        {
            var model = _adminNotesService.GetAllNotes();

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var model = _adminNotesService.GetAdminNote(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(AdminNoteDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _adminNotesService.UpdateNote(model);

            return RedirectToAction("Index");
        }


        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(AdminNoteDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            _adminNotesService.AddNote(model);

            return RedirectToAction("Index");
        }


        public ActionResult Delete(int id)
        {
            var model = _adminNotesService.GetAdminNote(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(AdminNoteDto model)
        {
            _adminNotesService.DeleteNote(model);

            return RedirectToAction("Index");
        }

    }
}