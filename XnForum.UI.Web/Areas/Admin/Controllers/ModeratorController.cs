﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XnForum.Services.Contracts.DTO.Moderator;
using XnForum.Services.Contracts.Interfaces;
using XnForum.UI.Web.Extensions;
using XnValidation.Validation;
using XnValidation.Validation.MVC;

namespace XnForum.UI.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ModeratorController : Controller
    {
        private IModeratorService _moderatorService;
        private IValidationDictionary _validationDictionary;

        public ModeratorController(IModeratorService moderatorService)
        {
            _moderatorService = moderatorService;
            _validationDictionary = new ModelStateWrapper(ModelState);
        }

        // GET: Admin/Moderator
        public ActionResult Index()
        {
            var model = _moderatorService.GetAllModerators();
            return View(model);
        }

        // GET: Admin/Moderator/Details/5
        public ActionResult Details(int id)
        {
            var model = _moderatorService.GetModerator(id);
            return View(model);
        }

        // GET: Admin/Moderator/Create
        public ActionResult Create()
        {
            var model = _moderatorService.GetNewModerator();

            return View(model);
        }

        // POST: Admin/Moderator/Create
        [HttpPost]
        public ActionResult Create(ModeratorCreateDto model)
        {
            if (!ModelState.IsValid)
            {
                model = Mapper.Map<ModeratorCreateDto, ModeratorCreateDto>(model, _moderatorService.GetNewModerator());
                return View(model);
            }

            model.ModelState = _validationDictionary;
            _moderatorService.AddModerator(model);

            if (!ModelState.IsValid)
            {
                model = Mapper.Map<ModeratorCreateDto, ModeratorCreateDto>(model, _moderatorService.GetNewModerator());
                return View(model);
            }

            return RedirectToAction("Index");
            
        }


        // GET: Admin/Moderator/Delete/5
        public ActionResult Delete(int id)
        {
            var model = _moderatorService.GetModerator(id);
            return View(model);
        }

        // POST: Admin/Moderator/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, ModeratorDisplayDto model)
        {
            _moderatorService.DeleteModerator(model);
            return RedirectToAction("Index");
        }
    }
}
