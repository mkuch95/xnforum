﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Interfaces;
using XnForum.UI.Web.Extensions;

namespace XnForum.UI.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class StructureController : Controller
    {
        private ICategoryService _categoryService;
        private IForumService _forumService;

        public StructureController(ICategoryService categoryService, IForumService forumService)
        {
            _categoryService = categoryService;
            _forumService = forumService;
        }

        // GET: Admin/Forum
        public ActionResult Index()
        {
            var model = _categoryService.GetVisibleCategories();

            return View(model);
        }

        //categories
        public ActionResult CategoryCreate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CategoryCreate(CategoryDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            _categoryService.AddCategory(model);

            return RedirectToAction("CategoryDetails", new {id = model.Id});
        }


        public ActionResult CategoryDetails(int id)
        {
            var model = _categoryService.GetCategory(id);

            return View(model);
        }


        public ActionResult CategoryEdit(int id)
        {
            var model = _categoryService.GetCategory(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult CategoryEdit(CategoryDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            _categoryService.UpdateCategory(model);

            return RedirectToAction("CategoryDetails", new { id = model.Id });
        }


        public ActionResult CategoryDelete(int id)
        {
            var model = _categoryService.GetCategory(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult CategoryDelete(CategoryDto model)
        {
            _categoryService.DeleteCategory(model);

            return RedirectToAction("Index");
        }

        //forums
        public ActionResult ForumCreate(int id)
        {
            var model = new ForumBaseDto();
            model.CategoryId = id;

            return View(model);
        }

        [HttpPost]
        public ActionResult ForumCreate(ForumBaseDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            _forumService.AddForum(model);

            return RedirectToAction("ForumDetails", new { id = model.Id });
        }


        public ActionResult ForumDetails(int id)
        {
            var model = _forumService.GetForumBase(id);

            return View(model);
        }


        public ActionResult ForumEdit(int id)
        {
            var model = _forumService.GetForumBase(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult ForumEdit(ForumBaseDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            _forumService.UpdateForum(model);

            return RedirectToAction("ForumDetails", new { id = model.Id });
        }


        public ActionResult ForumDelete(int id)
        {
            var model = _forumService.GetForumBase(id);

            return View(model);
        }

        [HttpPost]
        public ActionResult ForumDelete(ForumBaseDto model)
        {
            _forumService.DeleteForum(model);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "CategoryDown")]
        public ActionResult CategoryMoveDown(int catId)
        {
            _categoryService.MoveDown(catId);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "CategoryUp")]
        public ActionResult CategoryMoveUp(int catId)
        {
            _categoryService.MoveUp(catId);

            return RedirectToAction("Index");
        }
    }
}