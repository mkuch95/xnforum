﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;
using XnValidation.Validation;

namespace XnForum.UI.Web.Validation
{
    public class ModelStateWebWrapper : IValidationDictionary
    {
        private ModelStateDictionary _modelState;

        public ModelStateWebWrapper(ModelStateDictionary modelState)
        {
            _modelState = modelState;
        }

        public void AddError(string key, string errorMessage)
        {
            _modelState.AddModelError(key, errorMessage);
        }

        public bool IsValid
        {
            get { return _modelState.IsValid; }
        }
    }
}