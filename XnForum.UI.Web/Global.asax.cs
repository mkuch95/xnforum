﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using Microsoft.Owin;
using Newtonsoft.Json.Serialization;
using XnForum.UI.Web.Helpers;

namespace XnForum.UI.Web
{
    
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Mapper.Initialize(cfg => cfg.AddProfile<OrganizationProfile>());
            GlobalConfiguration.Configuration
                .ParameterBindingRules
                .Insert(0, SimplePostVariableParameterBinding.HookupParameterBinding);

            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            IoCConfig.RegisterDependencies();
            Log4netConfig.Config();
            GlobalConfiguration.Configuration.EnsureInitialized();

            
        }
    }
}
