﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Interfaces;
using XnForum.UI.Web.Extensions;

namespace XnForum.UI.Web.Controllers
{
    [Authorize]
    public class MessageController : Controller
    {
        private IMessageService _messageService;

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public ActionResult Messages(int page)
        {
            var model = _messageService.GetHeaderMessages(HttpContext.User.Identity.GetUserId<int>(), page);
            return View(model);
        }

        public ActionResult Talk(string login, int page)
        {
            var model = _messageService.GetTalk(HttpContext.User.Identity.GetUserId<int>(), login, page);
            return View(model);
        }

        [HttpPost]
        public ActionResult Talk(string login, int page, MessageTalkDto model)
        {
            var newMsg = model.MessageCreate;

            if (!ModelState.IsValid)
            {
                model = _messageService.GetTalk(HttpContext.User.Identity.GetUserId<int>(), login, page);
                model.MessageCreate = newMsg;
                return View(model);
            }

            newMsg.FromUserId = HttpContext.User.Identity.GetUserId<int>();
            newMsg.ToUserLogin = login;
            _messageService.AddMessage(newMsg);

            return RedirectToAction("Talk", new {login = login });
        }

        [HttpPost]
        public ActionResult DeleteMessage(string login, int page, int messageId)
        {
            _messageService.DeleteMessage(HttpContext.User.Identity.GetUserId<int>(), messageId);

            return RedirectToAction("Talk", new { login = login, page = page });
        }
        

    }
}