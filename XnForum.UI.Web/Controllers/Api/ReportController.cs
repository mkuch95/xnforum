﻿using System.Collections.Generic;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Interfaces;

namespace XnForum.UI.Web.Controllers.Api
{
    public class ReportController : ApiController
    {
        private IReportService _reportService;

        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }


        // GET: api/ApiReport
        [HttpGet]
        public IList<ReportReasonDto> Get()
        {
            var model = _reportService.GetAllReportReasons();

            return model;
        }

        [HttpPost]
        public string Post(int postId, int reasonId)
        {
            _reportService.ReportPost(User.Identity.GetUserId<int>(), postId, reasonId);

            return "dziękujemy za zgłoszenie";
        }
        
    }
}
