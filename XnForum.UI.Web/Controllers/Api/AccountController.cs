﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using XnForum.Entities.Model;
using XnForum.Services.Contracts.DTO.Account;
using XnForum.Services.Contracts.Interfaces;
using XnForum.UI.Web.Helpers;
using XnForum.UI.Web.Validation;
using XnValidation.Validation.MVC;

namespace XnForum.UI.Web.Controllers.Api
{
    public class AccountController : ApiController
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(AccountController));
        private IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        //[HttpPost]
        public IHttpActionResult Post(string login, string password)//not safe, I know..
        {
            Log.Info("login: " + (login ?? "null"));

            var user = new UserLoginDto();
            user.Login = login;
            user.Password = password;
            user.ModelState = new ModelStateWebWrapper(ModelState);
            var model = _accountService.Login(user);

            return Json(model, ViewHelpers.CamelCase);
        }
    }
}
