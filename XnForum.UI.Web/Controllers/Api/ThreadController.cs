﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Interfaces;
using XnForum.UI.Web.Helpers;
using XnForum.UI.Web.Validation;

namespace XnForum.UI.Web.Controllers.Api
{
    public class ThreadController : ApiController
    {
        private IThreadService _threadService;

        public ThreadController(IThreadService threadService)
        {
            _threadService = threadService;
        }

        public IHttpActionResult Get(int id)
        {
            var model = _threadService.GetThread(id);
            
            return Json(model, ViewHelpers.CamelCase);
        }

        public string Post(int id, int thread, string description)
        {
            var model = new PostCreateDto();
            model.UserId = id;
            model.Text = description;
            model.ThreadId = thread;
            model.ModelState = new ModelStateWebWrapper(ModelState);

            _threadService.AddPost(model);
            if (!ModelState.IsValid)
            {
                var errorList = ModelState.Values.SelectMany(m => m.Errors)
                                 .Select(e => e.ErrorMessage)
                                 .ToList();

                return String.Join(", ", errorList);
            }

            return "ok";
        }
    }
}
