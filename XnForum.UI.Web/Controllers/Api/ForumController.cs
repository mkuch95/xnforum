﻿using System.Web.Http;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Interfaces;
using XnForum.UI.Web.Helpers;

namespace XnForum.UI.Web.Controllers.Api
{
    public class ForumController : ApiController
    {
        private IForumService _forumService;

        public ForumController(IForumService forumService)
        {
            _forumService = forumService;
        }

        //main page
        public IHttpActionResult Get()
        {
            var model = _forumService.GetAllForumData();

            return Json(model, ViewHelpers.CamelCase);
        }

        //forum
        public IHttpActionResult Get(int id)
        {
            var model = _forumService.GetForum(id);

            return Json(model, ViewHelpers.CamelCase);
        }
        
    }
}