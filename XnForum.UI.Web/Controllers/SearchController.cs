﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XnForum.Services.Contracts.DTO.Search;
using XnForum.Services.Contracts.Interfaces;

namespace XnForum.UI.Web.Controllers
{
    public class SearchController : Controller
    {
        private ISearchService _searchService;

        public SearchController(ISearchService searchService)
        {
            _searchService = searchService;
        }

        public ActionResult Index()
        {
            var model = _searchService.GetSearchForm();
            return View(model);
        }

        public ActionResult Search(SearchFormDto model)
        {
            var search = _searchService.Search(model);
            return View(search);
        }
    }
}
