﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XnForum.Services.Contracts.Interfaces;

namespace XnForum.UI.Web.Controllers
{
    public class ForumController : Controller
    {
        private IForumService _forumService;

        public ForumController(IForumService forumService)
        {
            _forumService = forumService;
        }

        //main page
        public ActionResult Index()
        {
            var model = _forumService.GetAllForumData();

            return View(model);
        }

        //forum
        public ActionResult Forum(int id, int page)
        {
            var model = _forumService.GetForum(id, page);

            return View(model);
        }
        
    }
}