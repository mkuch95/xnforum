﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using XnForum.Services;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Interfaces;
using XnForum.UI.Web.Extensions;

namespace XnForum.UI.Web.Controllers
{
    public class ModeratorController : Controller
    {
        private IModeratorService _moderatorService;

        public ModeratorController(IModeratorService moderatorService)
        {
            _moderatorService = moderatorService;
        }



        // GET: Moderator/Forum/{forumid?}
        public ActionResult Reports(int? id)
        {
            var model = _moderatorService.GetModeratorPage(User.Identity.GetUserId<int>(), id);

            return View(model);
        }


        //post details & actions
        public ActionResult Post(int id)
        {
            var model = _moderatorService.GetPostToEdit(User.Identity.GetUserId<int>(), id);

            return View(model);
        }

        [HttpPost]
        public ActionResult Post(PostEditDto model)//saving
        {
            _moderatorService.EditPost(User.Identity.GetUserId<int>(), model);

            return RedirectToAction("Reports");
        }

        [HttpPost]
        public ActionResult PostDelete(int id)//delete
        {
            _moderatorService.DeletePost(User.Identity.GetUserId<int>(), id);

            return RedirectToAction("Reports");
        }

        [HttpPost]
        public ActionResult PostIgnore(int id)//ignore
        {
            _moderatorService.IgnorePostReports(User.Identity.GetUserId<int>(), id);

            return RedirectToAction("Reports");
        }

        public ActionResult History()
        {
            return View();
        }
    }
}