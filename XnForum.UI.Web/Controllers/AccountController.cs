﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.ClientServices;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.OAuth;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.DTO.Account;
using XnForum.Services.Contracts.Interfaces;
using Microsoft.Owin.Security;
using XnForum.UI.Web.Extensions;
using XnForum.UI.Web.Files;
using XnForum.UI.Web.Validation;
using XnValidation.Validation;
using XnValidation.Validation.MVC;

namespace XnForum.UI.Web.Controllers
{
    public class AccountController : Controller
    {
        private IAccountService _accountService;
        private IValidationDictionary _validationDictionary;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
            _validationDictionary = new ModelStateWrapper(ModelState);
        }


        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(UserLoginDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.ModelState = _validationDictionary;

            var user = _accountService.Login(model);

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Login),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Role, user.Role),
                new Claim("Avatar", user.Avatar)
            };


            var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

            Request.GetOwinContext().Authentication.SignIn(identity);
            
            return RedirectToAction("Index", "Forum");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(UserRegisterDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.ModelState = _validationDictionary;
            _accountService.Register(model);

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public ActionResult Logout()
        {
            Request.GetOwinContext().Authentication.SignOut();

            return RedirectToAction("Index", "Forum");
        }

        public ActionResult Account(string login)
        {
            var model = _accountService.GetUser(login);

            return View(model);
        }

        [Authorize]
        public ActionResult Settings()
        {
            var model = _accountService.GetUserSettings(User.Identity.GetUserId<int>());

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Settings(UserSettingsDto model, HttpPostedFileBase avatarUpload)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (avatarUpload != null)
                model.AvatarFile = avatarUpload.ToHttpInputFile();

            model.ModelState = new ModelStateWrapper(ModelState);
            _accountService.UpdateUserSettings(User.Identity.GetUserId<int>(), model);

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var settings = _accountService.GetUserSettings(User.Identity.GetUserId<int>());

            var identity = new ClaimsIdentity(User.Identity);
            identity.RemoveClaim(identity.FindFirst("Avatar"));
            identity.AddClaim(new Claim("Avatar", settings.Avatar));

            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            authenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(
                new ClaimsPrincipal(identity),
                new AuthenticationProperties() { IsPersistent = true }
                );

            //AuthenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant
            //(new ClaimsPrincipal(Identity), new AuthenticationProperties { IsPersistent = true });

            return RedirectToAction("Settings");//tmp
        }

        
    }
}