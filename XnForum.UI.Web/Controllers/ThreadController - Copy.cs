﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.DTO.Moderator;
using XnForum.Services.Contracts.Interfaces;
using XnForum.Services.Contracts.Validation;
using XnForum.UI.Web.Validation;

namespace XnForum.UI.Web.Controllers
{
    public class ThreadController : Controller
    {
        private IThreadService _threadService;
        private IValidationDictionary _validationDictionary;

        public ThreadController(IThreadService threadService)
        {
            _threadService = threadService;
            _validationDictionary = new ModelStateWrapper(ModelState);
        }

        public ActionResult Thread(int id, int page)
        {
            var model = _threadService.GetThread(id, page);

            return View(model);
        }

        [HttpPost]
        public ActionResult Thread(ThreadDisplayDto model)
        {
            var newPost = model.PostCreate;

            if (!ModelState.IsValid)
            {
                model = _threadService.GetThread(model.Id);
                model.PostCreate = newPost;

                return View(model);
            }

            newPost.UserId = HttpContext.User.Identity.GetUserId<int>();
            newPost.ThreadId = model.Id;
            newPost.ModelState = _validationDictionary;

            _threadService.AddPost(newPost);

            if (!ModelState.IsValid)
            {
                model = _threadService.GetThread(model.Id);
                model.PostCreate = newPost;

                return View(model);
            }

            //TODO przekierowanie do odpowiedniego postu... /page/3/#5343
            return RedirectToAction("Thread", new {id = model.Id});
        }

        //forumID
        public ActionResult Create(int id)
        {
            var model = _threadService.GetNewThread();
            model.ForumId = id;
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(int id, ThreadCreateDto model)
        {
            if (!ModelState.IsValid)
            {
                model = Mapper.Map<ThreadCreateDto, ThreadCreateDto>(model, _threadService.GetNewThread());
                return View(model);
            }

            model.UserId = HttpContext.User.Identity.GetUserId<int>();
            var threadId = _threadService.AddThread(model);

            return RedirectToAction("Thread", "Thread", new { id = threadId });
        }
    }
}