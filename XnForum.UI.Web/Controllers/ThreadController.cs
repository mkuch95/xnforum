﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.DTO.Moderator;
using XnForum.Services.Contracts.Interfaces;
using XnForum.UI.Web.Files;
using XnValidation.Validation;
using XnValidation.Validation.MVC;

namespace XnForum.UI.Web.Controllers
{
    public class ThreadController : Controller
    {
        private IThreadService _threadService;
        private IValidationDictionary _validationDictionary;

        public ThreadController(IThreadService threadService)
        {
            _threadService = threadService;
            _validationDictionary = new ModelStateWrapper(ModelState);
        }

        public ActionResult Thread(int id, int page)
        {
            var model = _threadService.GetThread(id, page);

            return View(model);
        }

        [HttpPost]
        public ActionResult Thread(PostCreateDto model, IEnumerable<HttpPostedFileBase> fileUpload)
        {
            if (!ModelState.IsValid)
            {
                var thread = _threadService.GetThread(model.ThreadId);
                thread.PostCreate = model;

                return View(thread);
            }

            model.UserId = HttpContext.User.Identity.GetUserId<int>();
            model.ModelState = _validationDictionary;
            if(fileUpload != null)
                model.Files = fileUpload.ToHttpInputFiles();

            _threadService.AddPost(model);

            if (!ModelState.IsValid)
            {
                var thread = _threadService.GetThread(model.ThreadId);
                thread.PostCreate = model;

                return View(thread);
            }
            
            //TODO redirect to post ex /page/3/#5343
            return RedirectToAction("Thread", new {id = model.ThreadId});
        }

        [Authorize]
        //forumID
        public ActionResult Create(int id)
        {
            var model = _threadService.GetNewThread();
            model.ForumId = id;
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(int id, ThreadCreateDto model, IEnumerable<HttpPostedFileBase> fileUpload)
        {
            if (!ModelState.IsValid)
            {
                model = Mapper.Map<ThreadCreateDto, ThreadCreateDto>(model, _threadService.GetNewThread());

                return View(model);
            }

            model.UserId = HttpContext.User.Identity.GetUserId<int>();
            model.ModelState = _validationDictionary;
            if (fileUpload != null)
                model.Files = fileUpload.ToHttpInputFiles();

            var threadId = _threadService.AddThread(model);


            if (!ModelState.IsValid)
            {
                model = Mapper.Map<ThreadCreateDto, ThreadCreateDto>(model, _threadService.GetNewThread());

                return View(model);
            }

            return RedirectToAction("Thread", "Thread", new { id = threadId });
        }
    }
}