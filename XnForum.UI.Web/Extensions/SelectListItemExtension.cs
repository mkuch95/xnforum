﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace XnForum.UI.Web.Extensions
{
    public static class SelectListItemExtension
    {
        public static IEnumerable<SelectListItem> ToSelectList<T>(this ICollection<T> @this, Func<T, object> value, Func<T, object> text)
        {
            foreach (var item in @this)
            {
                yield return new SelectListItem()
                {
                    Value = value(item).ToString(),
                    Text = text(item).ToString()
                };
            }
        }
    }
}