﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace XnForum.UI.Web.Extensions
{
    public static class HtmlEmoticonsExtension
    {
        private const string ImgHtml = "<img src=\"/Content/emots/{1}\" alt=\"{0}\" title=\"{0}\" />";

        public static string AddHtmlEmoticons(this string str)
        {
            StringBuilder sb = new StringBuilder(str);

            sb.Replace(":)", String.Format(ImgHtml, ":)", "5gkkhe6w.bmp"));
            sb.Replace(":D", String.Format(ImgHtml, ":D", "uqweupgi.bmp"));
            sb.Replace(":(", String.Format(ImgHtml, ":(", "69lszfjz.bmp"));
            sb.Replace(":P", String.Format(ImgHtml, ":P", "7nbcpar9.bmp"));
            sb.Replace(":p", String.Format(ImgHtml, ":p", "7nbcpar9.bmp"));
            sb.Replace(";)", String.Format(ImgHtml, ";)", "xzrtmdht.bmp"));
            sb.Replace(":*", String.Format(ImgHtml, ":*", "ut2wsk1n.bmp"));
            sb.Replace("^_^", String.Format(ImgHtml, "^_^", "trzght43.bmp"));
            sb.Replace(":v", String.Format(ImgHtml, ":v", "ndp165qh.bmp"));
            sb.Replace(":V", String.Format(ImgHtml, ":V", "ndp165qh.bmp"));
            sb.Replace("<3", String.Format(ImgHtml, "<3", "pe2l9x5a.bmp"));

            return sb.ToString();
        }
    }
}