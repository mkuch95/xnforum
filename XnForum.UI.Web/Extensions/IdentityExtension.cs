﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using Microsoft.Owin.Security;
using XnForum.Entities.Model;

namespace XnForum.UI.Web.Extensions
{
    public static class IdentityExtension
    {
        public static string GetAvatar(this IIdentity @this)
        {
            var iden = (ClaimsIdentity) @this;

            var claim = iden.Claims
                .SingleOrDefault(x => x.Type == "Avatar");

            if (claim != null)
                return claim.Value;

            return null;
        }
    }
}