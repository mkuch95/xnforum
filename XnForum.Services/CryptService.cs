﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using XnForum.Services.Contracts.Interfaces;

namespace XnForum.Services
{
    public class CryptService : ICryptService
    {
        private string Sha256(string text)
        {
            HashAlgorithm algorithm = new SHA256Managed();

            byte[] textBytes = new byte[text.Length];

            System.Buffer.BlockCopy(text.ToCharArray(), 0, textBytes, 0, textBytes.Length);

            byte[] coded = algorithm.ComputeHash(textBytes);

            return System.Text.Encoding.Default.GetString(coded);
        }

        public string GetRandomSalt()
        {
            return Sha256(Guid.NewGuid().ToString());
        }

        public string HashPassword(string password, string salt)
        {
            return Sha256(salt + password + salt);
        }

        public bool IsValid(string password, string salt, string passwordHash)
        {
            var hash = HashPassword(password, salt);

            return hash == passwordHash;
        }
    }
}
