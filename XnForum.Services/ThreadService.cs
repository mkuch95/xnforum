﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using NHibernate.Criterion;
using NHibernate.Criterion.Lambda;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using NHibernate;
using NHibernate.Linq;
using Repository.Pattern.UnitOfWork;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Interfaces;
using XnForum.Entities.Model;
using XnForum.Services.Contracts.Files;
using XnForum.Services.Exceptions;
using XnForum.Services.Extensions;
using XnValidation.Validation;

namespace XnForum.Services
{
    public class ThreadService : BaseService, IThreadService
    {
        public static readonly string UploadFilesDir = AppDomain.CurrentDomain.BaseDirectory + "/Content/PostFiles/";
        public const int PostsOnPage = 5;
        public const int MaxFileSize = 100 * 1024;
        public const int MaxFilesQuantity = 3;

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ThreadService));
        private IForumService _forumService;

        public ThreadService(IUnitOfWork unitOfWork, IForumService forumService) : base(unitOfWork)
        {
            _forumService = forumService;
        }

        public ThreadDisplayDto GetThread(int id, int page)
        {
            //one-to-many-to-many problem
            //http://www.philliphaydon.com/2011/04/13/nhibernate-querying-relationships-are-depth/
            var threadRepo = unitOfWork.Repository<Thread>();
            var postRepo = unitOfWork.Repository<Post>();

            var futureThread = threadRepo
                .QueryOver()
                .Fetch(x => x.Forum).Eager
                .Fetch(x => x.Forum.Category).Eager
                .Where(x => x.Id == id)
                .Future<Thread>();

            var futureThreadPost = threadRepo
                .QueryOver()
                .Fetch(x => x.Post).Eager
                .Fetch(x => x.Post.User).Eager
                .Fetch(x => x.Post.Files).Eager
                .Where(x => x.Id == id)
                .Future<Thread>();

            var thread = futureThread.SingleOrDefault();

            var futurePosts = postRepo
                .QueryOver()
                .Fetch(x => x.User).Eager
                .Where(x => x.Thread == thread)
                .Page(page, PostsOnPage)
                .Future<Post>();

            var futurePostsFiles = postRepo
                .QueryOver()
                .Fetch(x => x.Files).Eager
                .Where(x => x.Thread == thread)
                .Future<Post>();

            var postsCount = postRepo.QueryOver()
                .Where(x => x.Thread == thread)
                .RowCount();

            var posts = futurePosts.ToList();

            var model = Mapper.Map<ThreadDisplayDto>(thread);

            model.Page = page;
            model.PagesCount = (postsCount + PostsOnPage - 1) / PostsOnPage;

            model.Posts = Mapper.Map<IList<PostDisplayDto>>(posts);

            return model;
        }

        public ThreadDisplayDto GetThread(int id)
        {
            //one-to-many-to-many problem
            //http://www.philliphaydon.com/2011/04/13/nhibernate-querying-relationships-are-depth/
            var threadRepo = unitOfWork.Repository<Thread>();
            var postRepo = unitOfWork.Repository<Post>();

            var futureThread = threadRepo
                .QueryOver()
                .Fetch(x => x.Forum).Eager
                .Fetch(x => x.Forum.Category).Eager
                .Where(x => x.Id == id)
                .Future<Thread>();

            var futureThreadPost = threadRepo
                .QueryOver()
                .Fetch(x => x.Post).Eager
                .Fetch(x => x.Post.User).Eager
                .Fetch(x => x.Post.Files).Eager
                .Where(x => x.Id == id)
                .Future<Thread>();

            var thread = futureThread.SingleOrDefault();

            var futurePosts = postRepo
                .QueryOver()
                .Fetch(x => x.User).Eager
                .Where(x => x.Thread == thread)
                .Future<Post>();

            var futurePostsFiles = postRepo
                .QueryOver()
                .Fetch(x => x.Files).Eager
                .Where(x => x.Thread == thread)
                .Future<Post>();

            var postsCount = postRepo.QueryOver()
                .Where(x => x.Thread == thread)
                .RowCount();

            var posts = futurePosts.ToList();

            var model = Mapper.Map<ThreadDisplayDto>(thread);

            model.Posts = Mapper.Map<IList<PostDisplayDto>>(posts);

            return model;
        }

        


        public int AddThread(ThreadCreateDto thread)
        {
            var threadRepo = unitOfWork.Repository<Thread>();
            var userRepo = unitOfWork.Repository<User>();
            var postRepo = unitOfWork.Repository<Post>();
            var forumRepo = unitOfWork.Repository<Forum>();
            var postFileRepo = unitOfWork.Repository<PostFile>();


            var forum = forumRepo.GetById(thread.ForumId);
            var user = userRepo.GetById(thread.UserId);

            var postEntity = new Post()
            {
                Date = DateTime.Now,
                Text = SieveHtmlContent(thread.Text),
                User = user,
            };

            var threadEntity = new Thread()
            {
                Forum = forum,
                Post = postEntity,
                Name = thread.Name,
            };

            var badWords = GetBadWordsFromText(thread.Text);
            if (badWords.Any())
            {
                thread.AddError("Text", "Treść zawiera zakazane słowa: " + String.Join(", ", badWords));
                return -1;
            }

            var files = AddFiles(thread, thread.Files.ToList());
            if (files == null)
            {
                return -1;
            }
            files.ForEach(x => x.Post = postEntity);

            InTransaction(() =>
            {
                postRepo.Insert(postEntity);
                threadRepo.Insert(threadEntity);

                postFileRepo.InsertRange(files);
            });

            return threadEntity.Id;
        }

        public bool AddPost(PostCreateDto post)
        {
            var postRepo = unitOfWork.Repository<Post>();
            var postFileRepo = unitOfWork.Repository<PostFile>();
            var threadRepo = unitOfWork.Repository<Thread>();

            User user = null;
            if (post.UserId != null)
            {
                var userRepo = unitOfWork.Repository<User>();

                user = userRepo.GetById(post.UserId.Value);
            }

            var thread = threadRepo
                .QueryOver()
                .Where(x => x.Id == post.ThreadId)
                .Fetch(x => x.Forum).Eager
                .SingleOrDefault();

            if (user == null && !thread.Forum.AnonymousPosts)
            {
                throw new UserNotLoggedException();
            }

            var badWords = GetBadWordsFromText(post.Text);
            if (badWords.Any())
            {
                post.AddError("Text", "Treść zawiera zakazane słowa: "+ String.Join(", ", badWords));
                return false;
            }

            var entity = new Post
            {
                User = user,
                Text = SieveHtmlContent(post.Text),
                Date = DateTime.Now, //FIXME
                Thread = thread
            };

            var files = AddFiles(post, post.Files.ToList());
            if (files == null)
            {
                return false;
            }
            files.ForEach(x => x.Post = entity);


            InTransaction(() =>
                {
                    postRepo.Insert(entity);

                    postFileRepo.InsertRange(files);
                });

            return true;
        }

        public ThreadCreateDto GetNewThread()
        {
            var model = new ThreadCreateDto();

            model.Forums = _forumService.GetAllForums();

            return model;
        }

        private string SieveHtmlContent(string html)
        {
            var tagRepo = unitOfWork.Repository<HtmlTag>();

            var tags = tagRepo
                .Queryable()
                .Select(x => x.Tag)
                .ToList();

            string acceptable = String.Join("|", tags);// :/ fixit
            string stringPattern = @"</?(?(?=" + acceptable + @")notag|[a-zA-Z0-9]+)(?:\s[a-zA-Z0-9\-]+=?(?:(["",']?).*?\1?)?)*\s*/?>";
            return Regex.Replace(html, stringPattern, "");// bug "<b> tesxt..."
        }

        private IList<string> GetBadWordsFromText(string text)
        {
            var result = new List<string>();

            var badWordRepo = unitOfWork.Repository<BadWord>();

            var badWords = badWordRepo
                .Queryable()
                .Select(x => x.Value.ToLower())
                .ToList();

            text = text.ToLower();
            foreach (var word in badWords)
            {
                if (text.Contains(word))
                {
                    result.Add(word);
                }
            }

            return result;
        }

        private List<PostFile> AddFiles(IValidationModel model, IList<IInputFile> files)
        {
            var result = new List<PostFile>();

            if (files != null)
            {
                if (files.Count() > MaxFilesQuantity)
                {
                    model.AddError("Files", "Dodałeś za dużo plików! limit to: " + MaxFilesQuantity);
                    return null;
                }

                if (files.Any(x => x.ContentLength > MaxFileSize))
                {
                    model.AddError("Files", "Jeden z plików przekracza maksymalny rozmiar!");
                    return null;
                }

                if (files.Any())
                {
                    foreach (var file in files)
                    {
                        var fileName = MakeUnique(UploadFilesDir + file.FileName);
                        file.SaveAs(UploadFilesDir + fileName.Name);

                        result.Add(new PostFile
                        {
                            FileName = fileName.Name
                        });
                    }
                }
            }

            return result;
        }

        private FileInfo MakeUnique(string path)
        {
            string dir = Path.GetDirectoryName(path);
            string fileName = Path.GetFileNameWithoutExtension(path);
            string fileExt = Path.GetExtension(path);

            for (int i = 1; ; ++i)
            {
                if (!File.Exists(path))
                    return new FileInfo(path);

                path = Path.Combine(dir, fileName + " " + i + fileExt);
            }
        }
    }
}
