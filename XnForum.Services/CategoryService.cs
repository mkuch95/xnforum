﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using NHibernate.Linq;
using Repository.Pattern.UnitOfWork;
using XnForum.Entities.Model;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Interfaces;

namespace XnForum.Services
{
    public class CategoryService : BaseService, ICategoryService
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(CategoryService));

        public CategoryService(IUnitOfWork unitOfWork) : base(unitOfWork) { }

        public IList<CategoryDto> GetVisibleCategories()
        {
            var categoriesRepo = unitOfWork.Repository<Category>();

            var categories = categoriesRepo
                .Queryable()
                .Fetch(x => x.Forums)
                .OrderBy(x => x.Position)
                .ToList();

            return Mapper.Map<IList<CategoryDto>>(categories);
        }

        public IList<CategoryDto> GetAllCategories()
        {
            throw new NotImplementedException();
        }

        public CategoryDto GetCategory(int id)
        {
            var categoriesRepo = unitOfWork.Repository<Category>();

            var category = categoriesRepo
                .Queryable()
                .Fetch(x => x.Forums)
                .SingleOrDefault(x => x.Id == id);

            return Mapper.Map<CategoryDto>(category);
        }

        public void AddCategory(CategoryBaseDto category)
        {
            var categoryRepo = unitOfWork.Repository<Category>();

            var entity = new Category();
            entity.Name = category.Name;

            InTransaction(() =>
            {
                categoryRepo.Insert(entity);
                entity.Position = entity.Id;//można dodać triggera
                categoryRepo.Update(entity);
            });

            category.Id = entity.Id;
        }

        public void UpdateCategory(CategoryBaseDto category)
        {
            var categoryRepo = unitOfWork.Repository<Category>();

            var entity = categoryRepo
                .GetById(category.Id);

            entity.Name = category.Name;

            InTransaction(() =>
            {
                categoryRepo.Update(entity);
            });
        }

        public void DeleteCategory(CategoryBaseDto category)
        {
            var categoryRepo = unitOfWork.Repository<Category>();

            InTransaction(() =>
            {
                categoryRepo.Delete(category.Id);
            });
        }

        public void MoveUp(int categoryId)
        {
            var categoryRepo = unitOfWork.Repository<Category>();

            var entity1 = categoryRepo.Queryable()
                .SingleOrDefault(x => x.Id == categoryId);

            if (entity1 == null)
                return;

            var entity2 = categoryRepo.Queryable()
                .Where(x => x.Position < entity1.Position)
                .OrderByDescending(x => x.Position)
                .FirstOrDefault();

            if (entity2 == null)
                return;

            int pos = entity1.Position;
            entity1.Position = entity2.Position;
            entity2.Position = pos;

            InTransaction(() =>
            {
                categoryRepo.Update(entity1);
                categoryRepo.Update(entity2);
            });
        }

        public void MoveDown(int categoryId)
        {
            var categoryRepo = unitOfWork.Repository<Category>();

            var entity1 = categoryRepo.Queryable()
                .SingleOrDefault(x => x.Id == categoryId);

            if (entity1 == null)
                return;

            var entity2 = categoryRepo.Queryable()
                .Where(x => x.Position > entity1.Position)
                .OrderBy(x => x.Position)
                .FirstOrDefault();

            if (entity2 == null)
                return;

            int pos = entity1.Position;
            entity1.Position = entity2.Position;
            entity2.Position = pos;

            InTransaction(() =>
            {
                categoryRepo.Update(entity1);
                categoryRepo.Update(entity2);
            });
        }
    }
}
