﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using NHibernate.Criterion;
using Repository.Pattern.UnitOfWork;
using XnForum.Entities.Model;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.DTO.Moderator;
using XnForum.Services.Contracts.DTO.Report;
using XnForum.Services.Contracts.Enums;
using XnForum.Services.Contracts.Interfaces;
using XnValidation.Validation;

namespace XnForum.Services
{
    public class ModeratorService : BaseService, IModeratorService
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ModeratorService));
        private IForumService _forumService;
        private IAccountService _accountService;

        public ModeratorService(IUnitOfWork unitOfWork, IForumService forumService, IAccountService accountService) : base(unitOfWork)
        {
            _forumService = forumService;
            _accountService = accountService;
        }

        public void AddModerator(ModeratorCreateDto moderator)
        {
            var userRepo = unitOfWork.Repository<User>();
            var modRepo = unitOfWork.Repository<Moderator>();
            var forumRepo = unitOfWork.Repository<Forum>();

            var user = userRepo.GetById(moderator.UserId);
            var forum = forumRepo.GetById(moderator.ForumId);

            if (user == null)
            {
                moderator.AddError("UserId", "Użytkownik nie istnieje");
                return;
            }

            if (forum == null)
            {
                moderator.AddError("ForumId", "Forum nie istenije");
                return;
            }

            var mod = modRepo
                .Queryable()
                .Where(x => x.User == user)
                .SingleOrDefault(x => x.Forum == forum);

            if (mod != null)
            {
                moderator.AddError("", "Taki moderator już istnieje");
                return;
            }

            mod = new Moderator
            {
                User = user,
                Forum = forum,
                Date = DateTime.Now
            };

            InTransaction(() =>
            {
                modRepo.Insert(mod);
            });
        }

        public void DeleteModerator(ModeratorDisplayDto moderator)
        {
            var modRepo = unitOfWork.Repository<Moderator>();

            InTransaction(() =>
            {
                modRepo.Delete(moderator.Id);
            });
        }

        public IList<ModeratorDisplayDto> GetAllModerators()
        {
            var modRepo = unitOfWork.Repository<Moderator>();

            var all = modRepo.QueryOver()
                .Fetch(x => x.User).Eager
                .Fetch(x => x.Forum).Eager
                .List();

            return Mapper.Map<IList<ModeratorDisplayDto>>(all);
        }

        public IList<ModeratorDisplayDto> GetForumModerators(int forumId)
        {
            throw new NotImplementedException();
        }

        public ModeratorDisplayDto GetModerator(int id)
        {
            var modRepo = unitOfWork.Repository<Moderator>();

            var mod = modRepo.QueryOver()
                .Fetch(x => x.User).Eager
                .Fetch(x => x.Forum).Eager
                .Where(x => x.Id == id)
                .SingleOrDefault();

            return Mapper.Map<ModeratorDisplayDto>(mod);
        }

        public ModeratorCreateDto GetNewModerator()
        {
            var model = new ModeratorCreateDto();

            model.Users = _accountService.GetAllUsers();
            model.Forums = _forumService.GetAllForums();

            return model;
        }

        public IList<ReportedPostDto> GetReportedPosts(int forumId)
        {
            var reportRepo = unitOfWork.Repository<Report>();

            var reports = reportRepo
                .QueryOver()
                .Fetch(x => x.Post).Eager
                .Fetch(x => x.Post.Thread).Eager
                .Fetch(x => x.Post.Thread.Forum).Eager
                .Fetch(x => x.User).Eager
                .Fetch(x => x.Reason).Eager
                .Where(x => x.Status == ReportStatusEnum.Unverified)
                .JoinQueryOver(x => x.Post.Thread)
                    .Where(x => x.Forum.Id == forumId)
                .Future<Report>();

            /*
            var reportsPostFiles = reportRepo
                .QueryOver()
                .Fetch(x => x.Post).Eager
                .Fetch(x => x.Post.Files).Eager
                .Where(x => x.Status == ReportStatusEnum.Unverified)
                .JoinQueryOver(x => x.Post.Thread)
                    .Where(x => x.Forum.Id == forumId)
                .Future<Report>();
                */

            var dict = reports
                .ToList()
                .GroupBy(x => x.Post)
                .Select(x =>
                        new ReportedPostDto()
                        {
                            Post = Mapper.Map<PostDisplayDto>(x.Key),
                            Reports = Mapper.Map<IList<ReportDto>>(x.ToList())
                        }
                )
                .ToList();

            return dict;
        }

        public ModeratorReportPageDto GetModeratorPage(int userId, int? forumId)
        {
            var model = new ModeratorReportPageDto();
            model.Forums = GetModeratorForums(userId);

            if (forumId != null)
                model.ReportedPosts = GetReportedPosts(forumId.Value);

            return model;
        }

        public IList<ForumBaseDto> GetModeratorForums(int userId)
        {
            var modRepo = unitOfWork.Repository<Moderator>();

            var forums = modRepo
                .QueryOver()
                .Fetch(x => x.Forum).Eager
                .Fetch(x => x.Forum.Category).Eager
                .Where(x => x.User.Id == userId)
                .Select(x => x.Forum)
                .List<Forum>();


            return Mapper.Map<IList<ForumBaseDto>>(forums);
        }


        private bool CheckModAccess(int userId, int forumId)
        {
            var modRepo = unitOfWork.Repository<Moderator>();

            var mod = modRepo.QueryOver()
                .Where(x => x.User.Id == userId)
                .Where(x => x.Forum.Id == forumId)
                .SingleOrDefault();

            return mod != null;
        }

        public ReportedPostDto GetReportedPost(int userId, int postId)
        {
            var postRepo = unitOfWork.Repository<Post>();

            var post = postRepo
                .QueryOver()
                .Fetch(x => x.Thread).Eager
                .Where(x => x.Id == postId)
                .SingleOrDefault();

            if (!CheckModAccess(userId, post.Thread.Forum.Id))
                return null;

            return null;//fixme
        }

        public void DeletePost(int userId, int postId)
        {
            var postRepo = unitOfWork.Repository<Post>();
            var reportRepo = unitOfWork.Repository<Report>();

            var post = postRepo
                .QueryOver()
                .Fetch(x => x.Thread).Eager
                .Where(x => x.Id == postId)
                .SingleOrDefault();

            if (!CheckModAccess(userId, post.Thread.Forum.Id))
                return;

            var reports = reportRepo
                .QueryOver()
                .Where(x => x.Post == post)
                .Where(x => x.Status == ReportStatusEnum.Unverified)
                .List();

            post.Status = PostStatusEnum.DeletedByModerator;


            InTransaction(() =>
            {
                postRepo.Update(post);

                foreach (var report in reports)
                {
                    report.Status = ReportStatusEnum.Confirmed;
                    reportRepo.Update(report);
                }
            });
        }

        public void IgnorePostReports(int userId, int postId)
        {
            var postRepo = unitOfWork.Repository<Post>();
            var reportRepo = unitOfWork.Repository<Report>();

            var post = postRepo
                .QueryOver()
                .Fetch(x => x.Thread).Eager
                .Where(x => x.Id == postId)
                .SingleOrDefault();

            if (!CheckModAccess(userId, post.Thread.Forum.Id))
                return;

            var reports = reportRepo
                .QueryOver()
                .Where(x => x.Post == post)
                .Where(x => x.Status == ReportStatusEnum.Unverified)
                .List();


            InTransaction(() =>
            {
                foreach (var report in reports)
                {
                    report.Status = ReportStatusEnum.Ignored;
                    reportRepo.Update(report);
                }
            });
        }

        public PostEditDto GetPostToEdit(int userId, int postId)
        {
            var postRepo = unitOfWork.Repository<Post>();

            var postFuture = postRepo
                .QueryOver()
                .Fetch(x => x.Thread).Eager
                .Fetch(x => x.User).Eager
                .Where(x => x.Id == postId)
                .Future<Post>();

            var postFileFuture = postRepo
                .QueryOver()
                .Fetch(x => x.Files).Eager
                .Where(x => x.Id == postId)
                .Future<Post>();

            var post = postFuture.SingleOrDefault();

            if (!CheckModAccess(userId, post.Thread.Forum.Id))
                return null;

            return Mapper.Map<PostEditDto>(post);
        }

        public void EditPost(int userId, PostEditDto postModel)
        {
            var postRepo = unitOfWork.Repository<Post>();
            var reportRepo = unitOfWork.Repository<Report>();

            var postFuture = postRepo
                .QueryOver()
                .Fetch(x => x.Thread).Eager
                .Fetch(x => x.User).Eager
                .Where(x => x.Id == postModel.Id)
                .Future<Post>();
            //todo edit files

            var post = postFuture.SingleOrDefault();

            if (!CheckModAccess(userId, post.Thread.Forum.Id))
                return;

            post.Status = PostStatusEnum.EditedByModerator;
            post.Text = postModel.Text;

            var reports = reportRepo
                .QueryOver()
                .Where(x => x.Post == post)
                .Where(x => x.Status == ReportStatusEnum.Unverified)
                .List();


            InTransaction(() =>
            {
                postRepo.Update(post);
                foreach (var report in reports)
                {
                    report.Status = ReportStatusEnum.Ignored;
                    reportRepo.Update(report);
                }
            });
        }
    }
}
