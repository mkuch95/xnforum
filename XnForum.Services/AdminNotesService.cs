﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Remotion.Linq.Clauses.ResultOperators;
using Repository.Pattern.UnitOfWork;
using XnForum.Entities.Model;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Interfaces;

namespace XnForum.Services
{
    public class AdminNotesService : BaseService, IAdminNotesService
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(AdminNotesService));

        public AdminNotesService(IUnitOfWork unitOfWork) : base(unitOfWork) { }

        public void AddNote(AdminNoteDto adminNote)
        {
            var adminNotesRepo = unitOfWork.Repository<AdminNote>();

            var entity = new AdminNote();
            entity.Text = adminNote.Text;

            InTransaction(() =>
            {
                adminNotesRepo.Insert(entity);
            });

            adminNote.Id = entity.Id;
        }

        public void DeleteNote(AdminNoteDto adminNote)
        {
            var adminNotesRepo = unitOfWork.Repository<AdminNote>();

            InTransaction(() =>
            {
                adminNotesRepo.Delete(adminNote.Id);
            });
        }

        public AdminNoteDto GetAdminNote(int id)
        {
            var adminNotesRepo = unitOfWork.Repository<AdminNote>();

            var adminNote = adminNotesRepo
                .GetById(id);

            return Mapper.Map<AdminNoteDto>(adminNote);
        }

        public IList<AdminNoteDto> GetAllNotes()
        {
            var adminNotesRepo = unitOfWork.Repository<AdminNote>();

            var adminNotes = adminNotesRepo
                .Queryable()
                .ToList();

            return Mapper.Map<IList<AdminNoteDto>>(adminNotes);
        }

        public IList<AdminNoteDto> GetAllVisibleNotes()
        {
            var adminNotesRepo = unitOfWork.Repository<AdminNote>();

            var adminNotes = adminNotesRepo
                .Queryable()
                .ToList();

            return Mapper.Map<IList<AdminNoteDto>>(adminNotes);
        }

        public void UpdateNote(AdminNoteDto adminNote)
        {
            var adminNotesRepo = unitOfWork.Repository<AdminNote>();

            var entity = adminNotesRepo
                .GetById(adminNote.Id);

            entity.Text = adminNote.Text;

            InTransaction(() =>
            {
                adminNotesRepo.Update(entity);
            });
        }
    }
}
