﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Exceptions
{
    public class MessageToYourselfException : Exception
    {
        public MessageToYourselfException()
            : base ("You cant send message to yourself")
        {
        }
    }
}
