﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Exceptions
{
    public class UserNotExistException : Exception
    {
        public UserNotExistException(int id)
            : base ("User with id: " + id + " not exist")
        {
        }

        public UserNotExistException(string login)
            : base("User with login: " + login + " not exist")
        {
        }
    }
}
