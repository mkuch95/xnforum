﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XnForum.Services.Exceptions
{
    public class UserNotLoggedException : Exception
    {
        public UserNotLoggedException()
            : base ("User not logged!")
        {
        }
    }
}
