﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using NHibernate.Criterion;
using NHibernate.Linq;
using Repository.Pattern.UnitOfWork;
using XnForum.Entities.Model;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.DTO.Search;
using XnForum.Services.Contracts.Interfaces;

namespace XnForum.Services
{
    public class SearchService : BaseService, ISearchService
    {
        public const int MinWordSize = 2;

        private IForumService _forumService;
        public SearchService(IUnitOfWork unitOfWork, IForumService forumService) : base(unitOfWork)
        {
            _forumService = forumService;
        }

        public SearchFormDto GetSearchForm()
        {
            var model = new SearchFormDto();

            model.Forums = _forumService.GetAllForums();

            return model;
        }

        public SearchResultDto Search(SearchFormDto data)
        {
            var forumRepo = unitOfWork.Repository<Forum>();
            var postRepo = unitOfWork.Repository<Post>();
            var threadRepo = unitOfWork.Repository<Thread>();

            var model = new SearchResultDto
            {
                Forums = _forumService.GetAllForums(),
                ForumId = data.ForumId,
                Text = data.Text
            };

            bool searchSuperMode = false;

            var words = data.Text.Split(' ').ToList();
            words.RemoveAll(x => x.Length < MinWordSize);

            if (words.Any(x => x == "or" || x == "and" || x == "not"))
                searchSuperMode = true;

            Forum forum = null;
            if (data.ForumId != null)
                forum = forumRepo.GetById(data.ForumId.Value);

            //List<int> threadIds;
            var threads = threadRepo
                    .Queryable();

            if (forum != null)
            {
                threads = threads
                    .Where(x => x.Forum == forum);
            }

            var threadIds = threads
                .Select(x => x.Id)
                .ToList();
            var resultPosts = postRepo.QueryOver()
                .Fetch(x => x.User).Eager
                .WhereRestrictionOn(x => x.Thread.Id)
                .IsIn(threadIds)
                .List();

            var resultThreads = threads
                .Fetch(x => x.Post)
                .ToList();

            // :'( fixme search should be made in db mechanism, in query
            // now all threads are downloaded and checked, deadline...
            var postsTmp = resultPosts.ToList();
            var threadsTmp = resultThreads.ToList();

            if (!searchSuperMode)
            {
                postsTmp.RemoveAll(x => !IsValid(x.Text, words.ToArray()));
                threadsTmp.RemoveAll(x => !(
                    IsValid(x.Post.Text, words.ToArray())
                    ||
                    IsValid(x.Name, words.ToArray())
                ));
            }
            else
            {
                postsTmp.RemoveAll(x => !IsValidExtended(x.Text, data.Text));
                threadsTmp.RemoveAll(x => !(
                    IsValidExtended(x.Post.Text, data.Text)
                    ||
                    IsValidExtended(x.Name, data.Text)
                ));
            }

            resultPosts = postsTmp;
            resultThreads = threadsTmp;


            model.Posts = Mapper.Map<IList<PostDisplayDto>>(resultPosts);
            model.Threads = Mapper.Map<IList<ThreadDisplayDto>>(resultThreads);

            return model;
        }


        private bool IsValid(string text, string[] pattern)
        {
            return text
                       .ToLower()
                       .Split(' ')
                       .Intersect(pattern)
                       .Count() > 0;
        }

        private bool IsValidExtended(string text, string pattern)
        {
            var words = text.Split(' ');

            var patternList = new List<OrList>();
            var ors = pattern.Split(new string[] { "or" }, StringSplitOptions.None);
            foreach (var or in ors)
            {
                var orList = new OrList();
                patternList.Add(orList);
                //if size...
                var ands = or.Split(new string[] { "and" }, StringSplitOptions.None);

                foreach (var and in ands)
                {
                    var andList = new AndList();
                    orList.Ands.Add(andList);
                    
                    var pats = and.Split(' ');
                    bool neg = false;
                    foreach (var pat in pats)
                    {
                        if(pat.Length < MinWordSize)
                            continue;

                        if (pat == "not")
                        {
                            neg = true;
                            continue;
                        }

                        var word = new Word(pat, neg);
                        andList.Words.Add(word);
                    }
                }

            }


            foreach (var or in patternList)
            {
                bool result = true;

                foreach (var and in or.Ands)
                {
                    foreach (var word in and.Words)
                    {
                        if (word.Neg)
                        {
                            if (text.Contains(word.Name))
                                result = false;
                        }
                        else
                        {
                            if (!text.Contains(word.Name))
                                result = false;
                        }
                    }
                }

                if (result)
                    return true;
            }

            return false;
        }

        class OrList
        {
            public List<AndList> Ands { get; set; } = new List<AndList>();
        }

        class AndList
        {
            public List<Word> Words { get; set; } = new List<Word>();
        }

        class Word
        {
            public string Name { get; set; }
            public bool Neg { get; set; }

            public Word(string name, bool neg)
            {
                Name = name;
                Neg = neg;
            }
        }
    }
}
