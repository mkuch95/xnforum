﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Repository.Pattern.UnitOfWork;
using XnForum.Entities.Model;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Enums;
using XnForum.Services.Contracts.Interfaces;

namespace XnForum.Services
{
    public class ReportService : BaseService, IReportService
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ReportService));

        public ReportService(IUnitOfWork unitOfWork) : base(unitOfWork) { }


        public void AddReportReason(ReportReasonDto reportReason)
        {
            var reasonRepo = unitOfWork.Repository<ReportReason>();

            var entity = new ReportReason
            {
                Text = reportReason.Text
            };

            InTransaction(() =>
            {
                reasonRepo.Insert(entity);
            });
        }

        public void DeleteReportReason(ReportReasonDto reportReason)//soft delete
        {
            var reasonRepo = unitOfWork.Repository<ReportReason>();

            var entity = reasonRepo.GetById(reportReason.Id);

            entity.Deleted = true;

            InTransaction(() =>
            {
                reasonRepo.Update(entity);
            });
        }

        public void EditReportReason(ReportReasonDto reportReason)
        {
            var reasonRepo = unitOfWork.Repository<ReportReason>();

            var entity = reasonRepo.GetById(reportReason.Id);

            entity.Text = reportReason.Text;

            InTransaction(() =>
            {
                reasonRepo.Update(entity);
            });
        }

        public ReportReasonDto GetReportReason(int id)
        {
            var reasonRepo = unitOfWork.Repository<ReportReason>();

            var entity = reasonRepo.GetById(id);

            return Mapper.Map<ReportReasonDto>(entity);
        }

        public IList<ReportReasonDto> GetAllReportReasons()
        {
            var reasonRepo = unitOfWork.Repository<ReportReason>();

            var all = reasonRepo.Queryable()
                .Where(x => !x.Deleted)
                .ToList();

            return Mapper.Map<IList<ReportReasonDto>>(all);
        }

        public void ReportPost(int userId, int postId, int reasonId)
        {
            var userRepo = unitOfWork.Repository<User>();
            var reasonRepo = unitOfWork.Repository<ReportReason>();
            var postRepo = unitOfWork.Repository<Post>();
            var reportRepo = unitOfWork.Repository<Report>();

            var user = userRepo.GetById(userId);
            var reason = reasonRepo.GetById(reasonId);
            var post = postRepo.GetById(postId);

            if (user == null || reason == null || post == null)
                return;

            var report = new Report()
            {
                User = user,
                Post = post,
                Reason = reason,
                Status = ReportStatusEnum.Unverified
            };

            InTransaction(() =>
            {
                reportRepo.Insert(report);
            });
        }
    }
}
