﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using NHibernate.Criterion;
using Repository.Pattern.UnitOfWork;
using XnForum.Entities.Model;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Enums;
using XnForum.Services.Contracts.Interfaces;
using AutoMapper;
using NHibernate.Util;
using XnForum.Services.Contracts.DTO.Message;
using XnForum.Services.Exceptions;
using XnForum.Services.Extensions;

namespace XnForum.Services
{
    public class MessageService : BaseService, IMessageService
    {
        private const int TalksOnPage = 5;
        private const int MessagesOnPage = 5;

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(MessageService));

        public MessageService(IUnitOfWork unitOfWork) : base(unitOfWork) { }

        public void AddMessage(MessageCreateDto message)
        {
            var userRepo = unitOfWork.Repository<User>();
            var msgRepo = unitOfWork.Repository<Message>();

            var from = userRepo.GetById(message.FromUserId);
            var to = userRepo.Queryable()
                .SingleOrDefault(x => x.Login == message.ToUserLogin);

            if (from == null)
            {
                throw new UserNotExistException(message.FromUserId);
            }

            if (to == null)
            {
                throw new UserNotExistException(message.ToUserLogin);
            }

            if (to == from)
            {
                throw new MessageToYourselfException();
            }

            var entity = new Message
            {
                Text = message.Text,
                Date = DateTime.Now,
                From = from,
                To = to
            };

            InTransaction(() =>
            {
                msgRepo.Insert(entity);
            });
        }

        public void DeleteMessage(int userId, int messageId)
        {
            var msgRepo = unitOfWork.Repository<Message>();

            var msg = msgRepo.GetById(messageId);

            if (msg.To.Id == userId)
            {
                msg.ReceivedStatus = ReceivedMessageStatus.Deleted;
            }
            else if(msg.From.Id == userId)
            {
                msg.SentStatus = SentMessageStatus.Deleted;
            }

            InTransaction(() =>
            {
                msgRepo.Update(msg);
            });
        }

        public TalksListDto GetHeaderMessages(int userId, int page = 1)
        {
            var msgRepo = unitOfWork.Repository<Message>();

            var innerQuery = msgRepo.QueryOver()
                .Where(Restrictions.Or(
                    Restrictions.Conjunction()
                        .Add(Restrictions.Where<Message>(x => x.To.Id == userId))
                        .Add(Restrictions.Where<Message>(x => x.ReceivedStatus != ReceivedMessageStatus.Deleted))
                    ,
                    Restrictions.Conjunction()
                        .Add(Restrictions.Where<Message>(x => x.From.Id == userId))
                        .Add(Restrictions.Where<Message>(x => x.SentStatus != SentMessageStatus.Deleted))
                ))
                .SelectList(list => list
                        .SelectGroup(x => x.To.Id)
                        .SelectGroup(x => x.From.Id)
                        .SelectMax(x => x.Id)
                )
                .List<object[]>();//ToId, FromId, MsgId

            var ids = innerQuery
                .Select(x =>
                    {
                        var tmp = x.ToList();
                        tmp.Remove(userId);
                        return tmp.Select(y => (Int32) y).ToArray();
                    }
                ) //UserId, MsgId
                .GroupBy(x => x[0])
                .Select(grp => grp
                        .OrderByDescending(x => x[1])
                        .FirstOrDefault()
                )
                .Select(x => x[1]) // :/
                .ToArray();

            var headers = msgRepo.QueryOver()
                .Fetch(x => x.To).Eager
                .Fetch(x => x.From).Eager
                .Where(x => x.Id.IsIn(ids))
                .OrderBy(x => x.Date).Desc
                .Page(page, TalksOnPage);
                
            var headersList = headers.List();

            var model = new TalksListDto();
            model.Messages = Mapper.Map<IList<MessageHeaderDto>>(headersList);
            model.Page = page;
            model.PagesCount = (headers.RowCount() + TalksOnPage - 1) / TalksOnPage;

            foreach (var pair in headersList.Zip(model.Messages, Tuple.Create))
            {
                if (pair.Item1.To.Id == userId)
                {
                    pair.Item2.UserId = pair.Item1.From.Id;
                    pair.Item2.UserLogin = pair.Item1.From.Login;
                    pair.Item2.New = pair.Item1.ReceivedStatus == ReceivedMessageStatus.New;
                }
                else
                {
                    pair.Item2.UserId = pair.Item1.To.Id;
                    pair.Item2.UserLogin = pair.Item1.To.Login;
                }

                pair.Item2.ShortText = pair.Item1.Text.Substring(0, Math.Min(110, pair.Item1.Text.Length));//magic numbers
            }

            return model;
        }

        public MessageTalkDto GetTalk(int userId, string userLogin, int page = 1)
        {
            var userRepo = unitOfWork.Repository<User>();
            var msgRepo = unitOfWork.Repository<Message>();

            var userEntity = userRepo.Queryable()
                .SingleOrDefault(x=>x.Login == userLogin);

            if (userEntity == null)
            {
                throw new UserNotExistException(userLogin);
            }

            //UNION not supported by nHibernate
            var msgs = msgRepo.QueryOver()
                .Fetch(x => x.To).Eager
                .Fetch(x => x.From).Eager
                .Where(Restrictions.Or(
                    Restrictions.Conjunction()
                        .Add(Restrictions.Where<Message>(x => x.To.Id == userId))
                        .Add(Restrictions.Where<Message>(x => x.From == userEntity))
                        .Add(Restrictions.Where<Message>(x => x.ReceivedStatus != ReceivedMessageStatus.Deleted))
                    ,
                    Restrictions.Conjunction()
                        .Add(Restrictions.Where<Message>(x => x.To == userEntity))
                        .Add(Restrictions.Where<Message>(x => x.From.Id == userId))
                        .Add(Restrictions.Where<Message>(x => x.SentStatus != SentMessageStatus.Deleted))
                ))
                .OrderBy(x => x.Id).Desc // or x.Date
                .Page(page, TalksOnPage);

            var msgsList = msgs.List();
            var model = Mapper.Map<MessageTalkDto>(msgsList);


            model.Messages
                .Where(x => x.FromUserId == userId)
                .Where(x => x.New)
                .ForEach(x =>
                {
                    x.New = false;
                });

            model.Page = page;
            model.PagesCount = (msgs.RowCount() + MessagesOnPage - 1) / MessagesOnPage;
            model.UserLogin = userLogin;


            //if any message is new...
            if (msgsList.Any())
            {
                var newMsgs = msgsList.Where(x => x.To.Id == userId)
                    .Where(x => x.ReceivedStatus == ReceivedMessageStatus.New)
                    .ToList();

                if (newMsgs.Any())
                {
                    newMsgs.ForEach(x => x.ReceivedStatus = ReceivedMessageStatus.Readed);

                    InTransaction(() =>
                    {
                        foreach (var newMsg in newMsgs)
                        {
                            msgRepo.Update(newMsg);
                        }
                    });
                }
            }

            return model;
        }
    }
}
