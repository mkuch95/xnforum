﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.Pattern.UnitOfWork;
using XnForum.Entities.Model;
using XnForum.Services.Contracts.Interfaces;

namespace XnForum.Services
{
    public class AdminService : BaseService, IAdminService
    {
        public AdminService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public void AddBadWord(string text)
        {
            var badWordRepo = unitOfWork.Repository<BadWord>();

            var entity = badWordRepo
                .Queryable()
                .FirstOrDefault(x => x.Value == text);

            if(entity != null)
                return;

            var newEntity = new BadWord {Value = text};

            InTransaction(() =>
            {
                badWordRepo.Insert(newEntity);
            });
        }

        public void DeleteBadWord(string text)
        {
            var badWordRepo = unitOfWork.Repository<BadWord>();

            var entity = badWordRepo
                .Queryable()
                .FirstOrDefault(x => x.Value == text);

            InTransaction(() =>
            {
                badWordRepo.Delete(entity);
            });
        }

        public IList<string> GetAllBadWords()
        {
            var badWordRepo = unitOfWork.Repository<BadWord>();

            return badWordRepo.
                Queryable()
                .Select(x => x.Value)
                .ToList();
        }

        public IList<string> GetAllHtmlTags()
        {
            var tagRepo = unitOfWork.Repository<HtmlTag>();

            return tagRepo.
                Queryable()
                .Select(x => x.Tag)
                .ToList();
        }

        public void AddHtmlTag(string tag)
        {
            var tagRepo = unitOfWork.Repository<HtmlTag>();

            var entity = tagRepo
                .Queryable()
                .FirstOrDefault(x => x.Tag == tag);

            if (entity != null)
                return;

            var newEntity = new HtmlTag { Tag = tag};

            InTransaction(() =>
            {
                tagRepo.Insert(newEntity);
            });
        }

        public void DeleteHtmlTag(string tag)
        {
            var tagRepo = unitOfWork.Repository<HtmlTag>();

            var entity = tagRepo
                .Queryable()
                .FirstOrDefault(x => x.Tag == tag);

            InTransaction(() =>
            {
                tagRepo.Delete(entity);
            });
        }

        

        
    }
}
