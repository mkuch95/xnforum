﻿using Repository.Pattern.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace XnForum.Services
{
    public class BaseService
    {
        protected readonly IUnitOfWork unitOfWork;

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(BaseService));

        public BaseService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        
        protected void InTransaction(Action action)
        {
            try
            {
                unitOfWork.BeginTransaction();

                action();

                unitOfWork.Commit();
            }
            catch (Exception exception)
            {
                unitOfWork.Rollback();

                Log.Error("Transaction rollback", exception);

                throw;
            }
        }
        
    }
}
