﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Repository.Pattern.UnitOfWork;
using XnForum.Entities.Model;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.DTO.Account;
using XnForum.Services.Contracts.Enums;
using XnForum.Services.Contracts.Files;
using XnForum.Services.Contracts.Interfaces;
using XnValidation.Validation;

namespace XnForum.Services
{
    public class AccountService : BaseService, IAccountService
    {
        public static readonly string AvatarsDir = AppDomain.CurrentDomain.BaseDirectory + "/Content/Avatars/";
        public const int AvatarMaxSize = 100 * 1024;
        public const int AvatarMaxWidth = 400;
        public const int AvatarMaxHeight = 400;

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ForumService));
        private ICryptService _cryptService;

        public AccountService(IUnitOfWork unitOfWork, ICryptService cryptService) : base(unitOfWork)
        {
            _cryptService = cryptService;
        }

        public IList<UserDto> GetAllUsers()
        {
            var userRepo = unitOfWork.Repository<User>();
            var users = userRepo.Queryable();

            return Mapper.Map<IList<UserDto>>(users);
        }

        public UserDto GetUser(int id)
        {
            var userRepo = unitOfWork.Repository<User>();
            var user = userRepo.GetById(id);

            return Mapper.Map<UserDto>(user);
        }

        public UserDto GetUser(string login)
        {
            var userRepo = unitOfWork.Repository<User>();
            var user = userRepo.Queryable()
                .SingleOrDefault(x => x.Login == login);

            return Mapper.Map<UserDto>(user);
        }

        public UserSettingsDto GetUserSettings(int id)
        {
            var userRepo = unitOfWork.Repository<User>();
            var user = userRepo.GetById(id);

            return Mapper.Map<UserSettingsDto>(user);
        }

        public void UpdateUserSettings(int id, UserSettingsDto model)
        {
            var userRepo = unitOfWork.Repository<User>();
            var user = userRepo.GetById(id);

            if (!String.IsNullOrEmpty(model.Password))
            {
                user.Salt = _cryptService.GetRandomSalt();
                user.Password = _cryptService.HashPassword(model.Password, user.Salt);
            }

            user.Email = model.Email;

            if (model.AvatarFile != null)
            {
                if (model.AvatarFile.ContentLength > AvatarMaxSize)
                {
                    model.AddError("AvatarFile", "Plik przekrasza dopuszczalny rozmiar!");
                    return;
                }

                Image image = null;
                try
                {
                    image = System.Drawing.Image.FromStream(model.AvatarFile.InputStream);
                }
                catch (ArgumentException)
                {
                    model.AddError("AvatarFile", "Plik graficzny niepoprawny");
                    return;
                }

                if (image.Width > AvatarMaxWidth || image.Height > AvatarMaxHeight)
                {
                    model.AddError("AvatarFile",
                        "Plik ma za dużą rozdzielczość, maksymalna rozdzielczość to: " + AvatarMaxWidth + "x" +
                        AvatarMaxHeight);
                    return;
                }
                
                if (!String.IsNullOrEmpty(user.Avatar))
                    File.Delete(AvatarsDir + user.Avatar);

                var fileName = user.Id + Path.GetExtension(model.AvatarFile.FileName);
                image.Save(AvatarsDir + fileName);
                user.Avatar = fileName;
            }

            InTransaction(() =>
            {
                userRepo.Update(user);
            });
        }

        public UserSessionDataDto Login(UserLoginDto user)
        {
            var userRepo = unitOfWork.Repository<User>();

            var userEntity = userRepo.QueryOver()
                .Fetch(x => x.Moderators).Eager
                //.Where(x=>x.FacebookId == null) //fb account can have the same logins...
                .Where(x => x.Login == user.Login)
                .SingleOrDefault();

            if (userEntity == null)
            {
                user.AddError("Login", "User nie istnieje!");
                return null;
            }

            if (!_cryptService.IsValid(user.Password, userEntity.Salt, userEntity.Password))
            {
                user.AddError("Password", "Hasło nieprawidłowe");
                return null;
            }
            var model = Mapper.Map<UserSessionDataDto>(userEntity);

            if (userEntity.Role == UserRoleEnum.User && userEntity.Moderators.Any())
            {
                model.Role = "Moderator";
            }

            return model;
        }

        /*public UserSessionDataDto LoginByFacebook(string id, string name)
        {
            int fbId;
            try
            {
                fbId = Int32.Parse(id);
            }
            catch (FormatException)
            {
                Log.Error("fbid: " + id + " is not number");
                return null;
            }
            

            var userRepo = unitOfWork.Repository<User>();

            var userEntity = userRepo.Queryable()
                .SingleOrDefault(x => x.FacebookId == fbId);

            if (userEntity == null)
            {
                userEntity = new User();
                userEntity.Login = name;
                userEntity.FacebookId = fbId;

                InTransaction(() =>
                {
                      userRepo.Insert(userEntity);
                });
            }

            return Mapper.Map<UserSessionDataDto>(userEntity);
        }*/

        public bool Register(UserRegisterDto user)
        {
            var userRepo = unitOfWork.Repository<User>();

            var loginAvailable = userRepo
                .Queryable()
                .Any(x => x.Login.ToLower() == user.Login.ToLower());

            if (loginAvailable)
            {
                user.AddError("Login", "taki użytkownik już istnieje!");
                return false;
            }

            //zgodnosc hasel....
            var entity = new User();
            entity.Login = user.Login;
            entity.Salt = _cryptService.GetRandomSalt();
            entity.Password = _cryptService.HashPassword(user.Password, entity.Salt);
            entity.Email = user.Email;

            InTransaction(() =>
            {
                userRepo.Insert(entity);
            });

            return true;
        }
    }
}
