﻿using System;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using NHibernate.Linq;
using Repository.Pattern.UnitOfWork;
using XnForum.Entities.Model;
using XnForum.Services.Contracts.DTO;
using XnForum.Services.Contracts.Interfaces;
using XnForum.Services.Extensions;

namespace XnForum.Services
{
    public class ForumService : BaseService, IForumService
    {
        private const int ThreadsOnPage = 5;

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(ForumService));

        private IAdminNotesService _adminNotesService;
        private ICategoryService _categoryService;

        public ForumService(IUnitOfWork unitOfWork, IAdminNotesService adminNotesService, ICategoryService categoryService) : base(unitOfWork)
        {
            _adminNotesService = adminNotesService;
            _categoryService = categoryService;
        }

        public void AddForum(ForumBaseDto forum)
        {
            var forumRepo = unitOfWork.Repository<Forum>();
            var categoryRepo = unitOfWork.Repository<Category>();

            var entity = new Forum();
            entity.Name = forum.Name;
            entity.Category = categoryRepo.GetById(forum.Id);

            InTransaction(() =>
            {
                forumRepo.Insert(entity);
            });

            forum.Id = entity.Id;
        }

        public void DeleteForum(ForumBaseDto forum)
        {
            var forumRepo = unitOfWork.Repository<Forum>();

            InTransaction(() =>
            {
                forumRepo.Delete(forum.Id);
            });
        }

        public AllForumDataDto GetAllForumData()
        {
            var model = new AllForumDataDto
            {
                AdminNotes = _adminNotesService.GetAllVisibleNotes(),
                Categories = _categoryService.GetVisibleCategories()
            };

            return model;
        }

        public IList<ForumBaseDto> GetAllForums()
        {
            var forumRepo = unitOfWork.Repository<Forum>();

            var all = forumRepo.Queryable().ToList();

            return Mapper.Map<IList<ForumBaseDto>>(all);
        }

        public ForumDto GetForum(int id, int page)
        {
            var forumRepo = unitOfWork.Repository<Forum>();
            var threadRepo = unitOfWork.Repository<Thread>();

            var forum = forumRepo
                .Queryable()
                .Fetch(x => x.Category)
                .SingleOrDefault(x => x.Id == id);

            var threads = threadRepo
                .QueryOver()
                .Where(x => x.Forum == forum)
                .Page(page, ThreadsOnPage);

            var model = Mapper.Map<ForumDto>(forum);
            model.Threads = Mapper.Map<IList<ThreadBaseDto>>(threads.List());
            model.Page = page;
            model.PagesCount = (threads.RowCount() + ThreadsOnPage - 1) / ThreadsOnPage;
            return model;
        }

        public ForumDto GetForum(int id)
        {
            var forumRepo = unitOfWork.Repository<Forum>();
            var threadRepo = unitOfWork.Repository<Thread>();

            var forum = forumRepo
                .Queryable()
                .Fetch(x => x.Category)
                .SingleOrDefault(x => x.Id == id);

            var threads = threadRepo
                .QueryOver()
                .Where(x => x.Forum == forum);

            var model = Mapper.Map<ForumDto>(forum);
            model.Threads = Mapper.Map<IList<ThreadBaseDto>>(threads.List());
            return model;
        }

        public ForumBaseDto GetForumBase(int id)
        {
            var forumRepo = unitOfWork.Repository<Forum>();

            var forum = forumRepo
                .Queryable()
                .Fetch(x => x.Category)
                .SingleOrDefault(x => x.Id == id);

            return Mapper.Map<ForumBaseDto>(forum);
        }

        public void UpdateForum(ForumBaseDto forum)
        {
            var forumRepo = unitOfWork.Repository<Forum>();

            var entity = forumRepo
                .GetById(forum.Id);

            entity.Name = forum.Name;
            entity.AnonymousPosts = forum.AnonymousPosts;

            InTransaction(() =>
            {
                forumRepo.Update(entity);
            });
        }
    }
}
